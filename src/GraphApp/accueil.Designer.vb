﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class accueil
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lbl_name = New System.Windows.Forms.Label()
        Me.pnl_mode = New System.Windows.Forms.Panel()
        Me.rb_JoueurVsIaInt = New System.Windows.Forms.RadioButton()
        Me.rb_IAVsIA = New System.Windows.Forms.RadioButton()
        Me.rb_JoueurVsIAExt = New System.Windows.Forms.RadioButton()
        Me.btn_name = New System.Windows.Forms.Button()
        Me.rb_JoueurVsJoueur = New System.Windows.Forms.RadioButton()
        Me.pnl_setname = New System.Windows.Forms.Panel()
        Me.btn_retourMode = New System.Windows.Forms.Button()
        Me.btn_start = New System.Windows.Forms.Button()
        Me.txt_name2 = New System.Windows.Forms.TextBox()
        Me.txt_name1 = New System.Windows.Forms.TextBox()
        Me.lbl_name2 = New System.Windows.Forms.Label()
        Me.lbl_name1 = New System.Windows.Forms.Label()
        Me.lbl_setnameplayer = New System.Windows.Forms.Label()
        Me.pnl_choixcarte = New System.Windows.Forms.Panel()
        Me.lb_nameCarte = New System.Windows.Forms.ListBox()
        Me.btn_createmap = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_envoyer = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pnl_begin = New System.Windows.Forms.Panel()
        Me.btn_createcarte = New System.Windows.Forms.Button()
        Me.btn_jouer = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lbl_cartecree = New System.Windows.Forms.Label()
        Me.pnl_mode.SuspendLayout()
        Me.pnl_setname.SuspendLayout()
        Me.pnl_choixcarte.SuspendLayout()
        Me.pnl_begin.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_name
        '
        Me.lbl_name.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lbl_name.AutoSize = True
        Me.lbl_name.Font = New System.Drawing.Font("Arial", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_name.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lbl_name.Location = New System.Drawing.Point(333, 39)
        Me.lbl_name.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_name.Name = "lbl_name"
        Me.lbl_name.Size = New System.Drawing.Size(369, 55)
        Me.lbl_name.TabIndex = 0
        Me.lbl_name.Text = "Bataille du café"
        '
        'pnl_mode
        '
        Me.pnl_mode.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pnl_mode.Controls.Add(Me.rb_JoueurVsIaInt)
        Me.pnl_mode.Controls.Add(Me.rb_IAVsIA)
        Me.pnl_mode.Controls.Add(Me.rb_JoueurVsIAExt)
        Me.pnl_mode.Controls.Add(Me.btn_name)
        Me.pnl_mode.Controls.Add(Me.rb_JoueurVsJoueur)
        Me.pnl_mode.Location = New System.Drawing.Point(395, 178)
        Me.pnl_mode.Margin = New System.Windows.Forms.Padding(4)
        Me.pnl_mode.Name = "pnl_mode"
        Me.pnl_mode.Size = New System.Drawing.Size(267, 159)
        Me.pnl_mode.TabIndex = 1
        Me.pnl_mode.Visible = False
        '
        'rb_JoueurVsIaInt
        '
        Me.rb_JoueurVsIaInt.AutoSize = True
        Me.rb_JoueurVsIaInt.Location = New System.Drawing.Point(27, 33)
        Me.rb_JoueurVsIaInt.Margin = New System.Windows.Forms.Padding(4)
        Me.rb_JoueurVsIaInt.Name = "rb_JoueurVsIaInt"
        Me.rb_JoueurVsIaInt.Size = New System.Drawing.Size(163, 21)
        Me.rb_JoueurVsIaInt.TabIndex = 3
        Me.rb_JoueurVsIaInt.TabStop = True
        Me.rb_JoueurVsIaInt.Text = "Joueur vs I.A. Interne"
        Me.rb_JoueurVsIaInt.UseVisualStyleBackColor = True
        '
        'rb_IAVsIA
        '
        Me.rb_IAVsIA.AutoSize = True
        Me.rb_IAVsIA.Location = New System.Drawing.Point(27, 90)
        Me.rb_IAVsIA.Margin = New System.Windows.Forms.Padding(4)
        Me.rb_IAVsIA.Name = "rb_IAVsIA"
        Me.rb_IAVsIA.Size = New System.Drawing.Size(91, 21)
        Me.rb_IAVsIA.TabIndex = 2
        Me.rb_IAVsIA.TabStop = True
        Me.rb_IAVsIA.Text = "I.A. vs I.A."
        Me.rb_IAVsIA.UseVisualStyleBackColor = True
        '
        'rb_JoueurVsIAExt
        '
        Me.rb_JoueurVsIAExt.AutoSize = True
        Me.rb_JoueurVsIAExt.Location = New System.Drawing.Point(27, 62)
        Me.rb_JoueurVsIAExt.Margin = New System.Windows.Forms.Padding(4)
        Me.rb_JoueurVsIAExt.Name = "rb_JoueurVsIAExt"
        Me.rb_JoueurVsIAExt.Size = New System.Drawing.Size(167, 21)
        Me.rb_JoueurVsIAExt.TabIndex = 1
        Me.rb_JoueurVsIAExt.TabStop = True
        Me.rb_JoueurVsIAExt.Text = "Joueur vs I.A. Externe"
        Me.rb_JoueurVsIAExt.UseVisualStyleBackColor = True
        '
        'btn_name
        '
        Me.btn_name.Location = New System.Drawing.Point(67, 127)
        Me.btn_name.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_name.Name = "btn_name"
        Me.btn_name.Size = New System.Drawing.Size(100, 28)
        Me.btn_name.TabIndex = 2
        Me.btn_name.Text = "Lancer"
        Me.btn_name.UseVisualStyleBackColor = True
        '
        'rb_JoueurVsJoueur
        '
        Me.rb_JoueurVsJoueur.AutoSize = True
        Me.rb_JoueurVsJoueur.Location = New System.Drawing.Point(27, 4)
        Me.rb_JoueurVsJoueur.Margin = New System.Windows.Forms.Padding(4)
        Me.rb_JoueurVsJoueur.Name = "rb_JoueurVsJoueur"
        Me.rb_JoueurVsJoueur.Size = New System.Drawing.Size(139, 21)
        Me.rb_JoueurVsJoueur.TabIndex = 0
        Me.rb_JoueurVsJoueur.TabStop = True
        Me.rb_JoueurVsJoueur.Text = "Joueur vs Joueur"
        Me.rb_JoueurVsJoueur.UseVisualStyleBackColor = True
        '
        'pnl_setname
        '
        Me.pnl_setname.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pnl_setname.Controls.Add(Me.btn_retourMode)
        Me.pnl_setname.Controls.Add(Me.btn_start)
        Me.pnl_setname.Controls.Add(Me.txt_name2)
        Me.pnl_setname.Controls.Add(Me.txt_name1)
        Me.pnl_setname.Controls.Add(Me.lbl_name2)
        Me.pnl_setname.Controls.Add(Me.lbl_name1)
        Me.pnl_setname.Controls.Add(Me.lbl_setnameplayer)
        Me.pnl_setname.Location = New System.Drawing.Point(399, 185)
        Me.pnl_setname.Margin = New System.Windows.Forms.Padding(4)
        Me.pnl_setname.Name = "pnl_setname"
        Me.pnl_setname.Size = New System.Drawing.Size(267, 176)
        Me.pnl_setname.TabIndex = 3
        Me.pnl_setname.Visible = False
        '
        'btn_retourMode
        '
        Me.btn_retourMode.Location = New System.Drawing.Point(15, 124)
        Me.btn_retourMode.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_retourMode.Name = "btn_retourMode"
        Me.btn_retourMode.Size = New System.Drawing.Size(100, 28)
        Me.btn_retourMode.TabIndex = 10
        Me.btn_retourMode.Text = "Retour"
        Me.btn_retourMode.UseVisualStyleBackColor = True
        '
        'btn_start
        '
        Me.btn_start.Location = New System.Drawing.Point(137, 124)
        Me.btn_start.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_start.Name = "btn_start"
        Me.btn_start.Size = New System.Drawing.Size(100, 28)
        Me.btn_start.TabIndex = 9
        Me.btn_start.Text = "Start"
        Me.btn_start.UseVisualStyleBackColor = True
        '
        'txt_name2
        '
        Me.txt_name2.Location = New System.Drawing.Point(137, 95)
        Me.txt_name2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_name2.Name = "txt_name2"
        Me.txt_name2.Size = New System.Drawing.Size(100, 22)
        Me.txt_name2.TabIndex = 8
        '
        'txt_name1
        '
        Me.txt_name1.Location = New System.Drawing.Point(137, 50)
        Me.txt_name1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_name1.Name = "txt_name1"
        Me.txt_name1.Size = New System.Drawing.Size(100, 22)
        Me.txt_name1.TabIndex = 7
        '
        'lbl_name2
        '
        Me.lbl_name2.AutoSize = True
        Me.lbl_name2.Location = New System.Drawing.Point(12, 98)
        Me.lbl_name2.Name = "lbl_name2"
        Me.lbl_name2.Size = New System.Drawing.Size(125, 17)
        Me.lbl_name2.TabIndex = 6
        Me.lbl_name2.Text = "Nom du Joueur 2 :"
        '
        'lbl_name1
        '
        Me.lbl_name1.AutoSize = True
        Me.lbl_name1.Location = New System.Drawing.Point(12, 54)
        Me.lbl_name1.Name = "lbl_name1"
        Me.lbl_name1.Size = New System.Drawing.Size(125, 17)
        Me.lbl_name1.TabIndex = 5
        Me.lbl_name1.Text = "Nom du Joueur 1 :"
        '
        'lbl_setnameplayer
        '
        Me.lbl_setnameplayer.AutoSize = True
        Me.lbl_setnameplayer.Location = New System.Drawing.Point(12, 12)
        Me.lbl_setnameplayer.Name = "lbl_setnameplayer"
        Me.lbl_setnameplayer.Size = New System.Drawing.Size(244, 17)
        Me.lbl_setnameplayer.TabIndex = 4
        Me.lbl_setnameplayer.Text = "Veuillez saisir le nom des participants"
        '
        'pnl_choixcarte
        '
        Me.pnl_choixcarte.Controls.Add(Me.lb_nameCarte)
        Me.pnl_choixcarte.Controls.Add(Me.btn_createmap)
        Me.pnl_choixcarte.Controls.Add(Me.Label3)
        Me.pnl_choixcarte.Controls.Add(Me.btn_envoyer)
        Me.pnl_choixcarte.Controls.Add(Me.Label1)
        Me.pnl_choixcarte.Location = New System.Drawing.Point(319, 109)
        Me.pnl_choixcarte.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pnl_choixcarte.Name = "pnl_choixcarte"
        Me.pnl_choixcarte.Size = New System.Drawing.Size(440, 412)
        Me.pnl_choixcarte.TabIndex = 4
        Me.pnl_choixcarte.Visible = False
        '
        'lb_nameCarte
        '
        Me.lb_nameCarte.FormattingEnabled = True
        Me.lb_nameCarte.ItemHeight = 16
        Me.lb_nameCarte.Location = New System.Drawing.Point(21, 52)
        Me.lb_nameCarte.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lb_nameCarte.Name = "lb_nameCarte"
        Me.lb_nameCarte.Size = New System.Drawing.Size(400, 212)
        Me.lb_nameCarte.TabIndex = 8
        '
        'btn_createmap
        '
        Me.btn_createmap.Location = New System.Drawing.Point(165, 346)
        Me.btn_createmap.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_createmap.Name = "btn_createmap"
        Me.btn_createmap.Size = New System.Drawing.Size(100, 30)
        Me.btn_createmap.TabIndex = 7
        Me.btn_createmap.Text = "Création map"
        Me.btn_createmap.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(56, 318)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(314, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Ou designer votre propre map puis jouer dessus"
        '
        'btn_envoyer
        '
        Me.btn_envoyer.Location = New System.Drawing.Point(179, 282)
        Me.btn_envoyer.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_envoyer.Name = "btn_envoyer"
        Me.btn_envoyer.Size = New System.Drawing.Size(75, 23)
        Me.btn_envoyer.TabIndex = 5
        Me.btn_envoyer.Text = "Envoyer"
        Me.btn_envoyer.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(127, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(184, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Choississez la carte de jeu :"
        '
        'pnl_begin
        '
        Me.pnl_begin.Controls.Add(Me.btn_createcarte)
        Me.pnl_begin.Controls.Add(Me.btn_jouer)
        Me.pnl_begin.Controls.Add(Me.Label4)
        Me.pnl_begin.Location = New System.Drawing.Point(436, 225)
        Me.pnl_begin.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pnl_begin.Name = "pnl_begin"
        Me.pnl_begin.Size = New System.Drawing.Size(200, 100)
        Me.pnl_begin.TabIndex = 5
        '
        'btn_createcarte
        '
        Me.btn_createcarte.Location = New System.Drawing.Point(29, 58)
        Me.btn_createcarte.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_createcarte.Name = "btn_createcarte"
        Me.btn_createcarte.Size = New System.Drawing.Size(119, 23)
        Me.btn_createcarte.TabIndex = 2
        Me.btn_createcarte.Text = "Créer une carte"
        Me.btn_createcarte.UseVisualStyleBackColor = True
        '
        'btn_jouer
        '
        Me.btn_jouer.Location = New System.Drawing.Point(53, 30)
        Me.btn_jouer.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_jouer.Name = "btn_jouer"
        Me.btn_jouer.Size = New System.Drawing.Size(75, 23)
        Me.btn_jouer.TabIndex = 1
        Me.btn_jouer.Text = "Jouer"
        Me.btn_jouer.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(51, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 17)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Je souhaite :"
        '
        'lbl_cartecree
        '
        Me.lbl_cartecree.AutoSize = True
        Me.lbl_cartecree.Location = New System.Drawing.Point(451, 90)
        Me.lbl_cartecree.Name = "lbl_cartecree"
        Me.lbl_cartecree.Size = New System.Drawing.Size(185, 17)
        Me.lbl_cartecree.TabIndex = 6
        Me.lbl_cartecree.Text = "Votre carte a bien été créée"
        Me.lbl_cartecree.Visible = False
        '
        'accueil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1067, 554)
        Me.Controls.Add(Me.lbl_cartecree)
        Me.Controls.Add(Me.pnl_begin)
        Me.Controls.Add(Me.pnl_choixcarte)
        Me.Controls.Add(Me.lbl_name)
        Me.Controls.Add(Me.pnl_mode)
        Me.Controls.Add(Me.pnl_setname)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "accueil"
        Me.Text = "Accueil"
        Me.pnl_mode.ResumeLayout(False)
        Me.pnl_mode.PerformLayout()
        Me.pnl_setname.ResumeLayout(False)
        Me.pnl_setname.PerformLayout()
        Me.pnl_choixcarte.ResumeLayout(False)
        Me.pnl_choixcarte.PerformLayout()
        Me.pnl_begin.ResumeLayout(False)
        Me.pnl_begin.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_name As Label
    Friend WithEvents pnl_mode As Panel
    Friend WithEvents rb_JoueurVsIAExt As RadioButton
    Friend WithEvents rb_JoueurVsJoueur As RadioButton
    Friend WithEvents rb_IAVsIA As RadioButton
    Friend WithEvents btn_name As Button
    Friend WithEvents pnl_setname As Panel
    Friend WithEvents txt_name2 As TextBox
    Friend WithEvents txt_name1 As TextBox
    Friend WithEvents lbl_name2 As Label
    Friend WithEvents lbl_name1 As Label
    Friend WithEvents lbl_setnameplayer As Label
    Friend WithEvents btn_start As Button
    Friend WithEvents pnl_choixcarte As Panel
    Friend WithEvents btn_envoyer As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents btn_retourMode As Button
    Friend WithEvents btn_createmap As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents pnl_begin As Panel
    Friend WithEvents btn_createcarte As Button
    Friend WithEvents btn_jouer As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents lbl_cartecree As Label
    Friend WithEvents lb_nameCarte As ListBox
    Friend WithEvents rb_JoueurVsIaInt As RadioButton
End Class
