﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class resultat
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnl_result = New System.Windows.Forms.Panel()
        Me.btn_accueil = New System.Windows.Forms.Button()
        Me.lbl_dernierscore = New System.Windows.Forms.Label()
        Me.lbl_premierscore = New System.Windows.Forms.Label()
        Me.lbl_dernier = New System.Windows.Forms.Label()
        Me.lbl_premier = New System.Windows.Forms.Label()
        Me.lbl_result = New System.Windows.Forms.Label()
        Me.pnl_result.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnl_result
        '
        Me.pnl_result.Controls.Add(Me.btn_accueil)
        Me.pnl_result.Controls.Add(Me.lbl_dernierscore)
        Me.pnl_result.Controls.Add(Me.lbl_premierscore)
        Me.pnl_result.Controls.Add(Me.lbl_dernier)
        Me.pnl_result.Controls.Add(Me.lbl_premier)
        Me.pnl_result.Controls.Add(Me.lbl_result)
        Me.pnl_result.Location = New System.Drawing.Point(78, 76)
        Me.pnl_result.Name = "pnl_result"
        Me.pnl_result.Size = New System.Drawing.Size(658, 295)
        Me.pnl_result.TabIndex = 0
        '
        'btn_accueil
        '
        Me.btn_accueil.Location = New System.Drawing.Point(270, 253)
        Me.btn_accueil.Name = "btn_accueil"
        Me.btn_accueil.Size = New System.Drawing.Size(75, 23)
        Me.btn_accueil.TabIndex = 5
        Me.btn_accueil.Text = "Accueil"
        Me.btn_accueil.UseVisualStyleBackColor = True
        '
        'lbl_dernierscore
        '
        Me.lbl_dernierscore.AutoSize = True
        Me.lbl_dernierscore.Location = New System.Drawing.Point(69, 199)
        Me.lbl_dernierscore.Name = "lbl_dernierscore"
        Me.lbl_dernierscore.Size = New System.Drawing.Size(73, 17)
        Me.lbl_dernierscore.TabIndex = 4
        Me.lbl_dernierscore.Text = "• Joueur 1"
        '
        'lbl_premierscore
        '
        Me.lbl_premierscore.AutoSize = True
        Me.lbl_premierscore.Location = New System.Drawing.Point(69, 111)
        Me.lbl_premierscore.Name = "lbl_premierscore"
        Me.lbl_premierscore.Size = New System.Drawing.Size(73, 17)
        Me.lbl_premierscore.TabIndex = 3
        Me.lbl_premierscore.Text = "• Joueur 1"
        '
        'lbl_dernier
        '
        Me.lbl_dernier.AutoSize = True
        Me.lbl_dernier.Location = New System.Drawing.Point(221, 168)
        Me.lbl_dernier.Name = "lbl_dernier"
        Me.lbl_dernier.Size = New System.Drawing.Size(73, 17)
        Me.lbl_dernier.TabIndex = 2
        Me.lbl_dernier.Text = "• Joueur 1"
        '
        'lbl_premier
        '
        Me.lbl_premier.AutoSize = True
        Me.lbl_premier.Location = New System.Drawing.Point(221, 80)
        Me.lbl_premier.Name = "lbl_premier"
        Me.lbl_premier.Size = New System.Drawing.Size(73, 17)
        Me.lbl_premier.TabIndex = 1
        Me.lbl_premier.Text = "• Joueur 1"
        '
        'lbl_result
        '
        Me.lbl_result.AutoSize = True
        Me.lbl_result.Location = New System.Drawing.Point(267, 16)
        Me.lbl_result.Name = "lbl_result"
        Me.lbl_result.Size = New System.Drawing.Size(81, 17)
        Me.lbl_result.TabIndex = 0
        Me.lbl_result.Text = "Classement"
        '
        'resultat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.pnl_result)
        Me.Name = "resultat"
        Me.Text = "Form1"
        Me.pnl_result.ResumeLayout(False)
        Me.pnl_result.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnl_result As Panel
    Friend WithEvents lbl_dernier As Label
    Friend WithEvents lbl_premier As Label
    Friend WithEvents lbl_result As Label
    Friend WithEvents lbl_dernierscore As Label
    Friend WithEvents lbl_premierscore As Label
    Friend WithEvents btn_accueil As Button
End Class
