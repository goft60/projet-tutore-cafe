﻿Imports CoffeeBattle
Imports System.IO
Imports System.Text
Imports System.Net.Sockets
Public Class accueil
    Dim Mode As ModeDeJeu
    ReadOnly Path As String = "..\..\..\Cartes.txt"
    Private Sub Btn_name_Click(sender As Object, e As EventArgs) Handles btn_name.Click
        pnl_mode.Visible = False
        pnl_setname.Visible = True
        If (rb_JoueurVsIAExt.Checked Or rb_JoueurVsIaInt.Checked) Then
            lbl_name1.Text = "Nom du Joueur :"
            lbl_name2.Text = "Nom de l'I.A. :"
        ElseIf (rb_IAVsIA.Checked) Then
            lbl_name1.Text = "Nom de l'I.A. 1 :"
            lbl_name2.Text = "Nom de l'I.A. 2 :"
        Else
            lbl_name1.Text = "Nom du Joueur 1 :"
            lbl_name2.Text = "Nom du Joueur 2 :"
        End If
    End Sub

    Private Sub btn_start_Click(sender As Object, e As EventArgs) Handles btn_start.Click
        If (txt_name1.Text IsNot "" And txt_name2.Text IsNot "") Then
            If (rb_IAVsIA.Checked) Then
                Mode = ModeDeJeu.IAVsIA
                lancementPartie("")
            ElseIf (rb_JoueurVsIAExt.Checked) Then
                Mode = ModeDeJeu.JoueurVsIAExt
                lancementPartie("")
            Else
                If rb_JoueurVsJoueur.Checked Then
                    Mode = ModeDeJeu.JoueurVsJoueur
                Else
                    Mode = ModeDeJeu.JoueurVsIAInt
                End If
                pnl_setname.Visible = False
                pnl_choixcarte.Visible = True
            End If
        Else
            MsgBox("Veuillez saisir les noms de tous les participants !", MsgBoxStyle.Exclamation, "Saisi nom")
        End If
    End Sub

    Private Sub initLb_nameCarte()

        If Not File.Exists(Path) Then
            Dim fs As FileStream
            fs = File.Create(Path)
            Dim Scabb = Encoding.ASCII.GetBytes("3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44|")
            Dim Phatt = Encoding.ASCII.GetBytes("67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|")
            Dim futur = Encoding.ASCII.GetBytes(String.Format("Scatt%{0}<Phatt%{1}<", Scabb, Phatt))
            fs.Write(futur, 0, futur.Length)
            fs.Close()
        End If
        readMapFile()
        lb_nameCarte.SetSelected(0, True)
    End Sub
    Private Sub readMapFile()
        Dim temp As String = File.ReadAllText(Path)
        Dim str = temp.Split("<")
        If lb_nameCarte.Items.Count = 0 Then
            lb_nameCarte.BeginUpdate()
            For Each carte As String In str
                Dim name = carte.Split("%")
                If Not carte.Contains(vbNullChar) And carte <> "" Then
                    lb_nameCarte.Items.Add(name(0))
                End If
            Next
            lb_nameCarte.EndUpdate()
        End If
    End Sub
    Public Function getNameCarte()
        Return Serveur.Game.Map.MapName
    End Function
    Public Sub addCarte(sentence)
        Dim to_add As New String(sentence)
        lb_nameCarte.BeginUpdate()
        lb_nameCarte.Items.Add(to_add)
        lb_nameCarte.EndUpdate()
    End Sub

    Public Function codeCarteNamed(sentence)
        Dim res = ""
        Dim temp = File.ReadAllText(Path)
        Dim str = temp.Split("<")
        For Each carte As String In str
            Dim name = carte.Split("%")
            If sentence = name(0) Then
                res = name(1)
            End If
        Next
        Return res
    End Function

    Private Sub lancementPartie(carte)
        Try
            Serveur.BeginGame(Mode, carte)
            Serveur.Game.GameMode = Mode
            If (ModeDeJeu.IAVsIA <> Serveur.Game.GameMode And ModeDeJeu.JoueurVsIAInt <> Serveur.Game.GameMode) Then
                Serveur.Game.InitPlayers(txt_name1.Text, txt_name2.Text)
            Else
                Serveur.Game.InitIa(txt_name1.Text, txt_name2.Text)
            End If
            If rb_JoueurVsJoueur.Checked Then
                Serveur.Game.Map.MapName = lb_nameCarte.SelectedItem.ToString
            End If
            plateauJeu.Show()
            Me.Hide()
        Catch ex As SocketException
            MsgBox(String.Format("La connexion au serveur {0}:{1} a échoué, veuillez réessayer ou changer de mode de jeu.", Serveur.GetHostIp(), Serveur.GetPort()), vbCritical, "Erreur de connexion au serveur")
        Catch ex As Exception
            MsgBox("La trame envoyée n'est pas valide, veuillez en saisir une autre ou choisir une carte prédéfinie", vbExclamation, "Erreur de trame")
        End Try
    End Sub

    Private Sub btn_envoyer_Click(sender As Object, e As EventArgs) Handles btn_envoyer.Click
        Dim carte = codeCarteNamed(lb_nameCarte.SelectedItem.ToString)
        lancementPartie(carte)
    End Sub

    Private Sub btn_retourMode_Click(sender As Object, e As EventArgs) Handles btn_retourMode.Click
        pnl_setname.Visible = False
        pnl_mode.Visible = True
    End Sub

    Private Sub MapCreator(sender As Object, e As EventArgs) Handles btn_createmap.Click, btn_createcarte.Click
        plateauCreation.Show()
        Me.Hide()
    End Sub

    Private Sub btn_jouer_Click(sender As Object, e As EventArgs) Handles btn_jouer.Click
        pnl_mode.Visible = True
        pnl_begin.Visible = False
    End Sub

    Private Sub Accueil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        initLb_nameCarte()
    End Sub

End Class