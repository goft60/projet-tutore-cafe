﻿Imports CoffeeBattle
Imports System.IO
Imports System.Text
Public Class plateauCreation
    ReadOnly map As New Map()
    ReadOnly sizePB As Double = 75
    ReadOnly modelItem_format = "Parcelle de {0} cases : {1} restante"
    Private Sub plateauCreation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        initPanelIlot()
        For indice As Int16 = 0 To 3
            lb_parcel.Items.Add(String.Format(modelItem_format, map.NbParcel.ToList.Item(indice).Key, map.NbParcel.ToList.Item(indice).Value))
        Next
        lb_parcel.SetSelected(0, True)
        accueil.Close()
    End Sub
    Private Sub initPanelIlot()
        pnl_ilot.Height = sizePB * 10
        pnl_ilot.Width = sizePB * 10
        For Each line As List(Of [Case]) In map.MapGrid
            For Each unit As [Case] In line
                Dim pic As New PictureBox With {
                    .Size = New Size(New Point(sizePB, sizePB)),
                    .Name = String.Format("pic_{0}_{1}", unit.Column, unit.Line),
                    .BorderStyle = BorderStyle.FixedSingle,
                    .BackColor = Color.Transparent,
                    .Location = New Point(unit.Column * sizePB, unit.Line * sizePB),
                    .Tag = ""
                }
                pnl_ilot.Controls.Add(pic)
            Next
        Next
    End Sub
    Private Sub editLbParcel()
        Dim i = 0
        For indice As Int16 = 0 To lb_parcel.Items.Count - 1
            lb_parcel.Items.RemoveAt(0)
        Next
        For Each keyvalue As KeyValuePair(Of Integer, Integer) In map.NbParcel.ToList
            If (keyvalue.Value = 0) Then
                map.NbParcel.Remove(keyvalue.Key)
            Else
                lb_parcel.Items.Add(String.Format(modelItem_format, map.NbParcel.ToList.Item(i).Key, map.NbParcel.ToList.Item(i).Value))
                i += 1
            End If
        Next
        If (lb_parcel.Items.Count <> 0) Then
            lb_parcel.SetSelected(0, True)
        End If
    End Sub
    Private Sub btn_creation_Click(sender As Object, e As EventArgs) Handles btn_creation.Click
        lb_parcel.Visible = False
        btn_creation.Visible = False
        btn_back.Visible = True
        btn_save.Visible = True
        lbl_creation.Visible = True
        lbl_creation.Text = String.Format("Création d'une parcelle de {0} cases", lb_parcel.SelectedItem.ToString.Split(" ")(2))

        map.ParcelToAdd = New Parcelle()
        For Each pic As PictureBox In pnl_ilot.Controls
            If (pic.BackColor = Color.Transparent And pic.Tag <> "addHandler") Then
                pic.Tag = "addHandler"
                AddHandler pic.Click, AddressOf pic_click
            End If
        Next
    End Sub
    Public Sub findIDColumnLineFromPic(Pic, ByRef id_col, ByRef id_line)
        Dim splited As String() = Pic.Name.Split("_")
        id_col = Integer.Parse(splited(1))
        id_line = Integer.Parse(splited(2))
    End Sub
    Private Sub pic_click(sender As Object, e As EventArgs)
        Dim pic As PictureBox = sender
        Dim id_column, id_line As Integer
        findIDColumnLineFromPic(pic, id_column, id_line)
        Dim unit = map.MapGrid.Item(id_line).Item(id_column)
        If (map.ParcelToAdd.Unites.Remove(unit)) Then
            unit.Parcel = Nothing
        Else
            map.ParcelToAdd.Unites.Add(unit)
            unit.Parcel = map.ParcelToAdd
        End If
        map.EditCaseClickable()
        editCaseCliquable(map.ParcelToAdd.Unites.Count)
    End Sub
    Private Sub plateauClickable()
        For Each pic As PictureBox In pnl_ilot.Controls
            If (pic.BackColor = Color.Red Or pic.BackColor = Color.Gold) Then
                pic.BackColor = Color.Transparent
            End If
            If pic.BackColor = Color.Transparent And pic.Tag <> "addHandler" Then
                AddHandler pic.Click, AddressOf pic_click
            End If
            pic.Tag = "addHandler"
        Next
    End Sub
    Private Sub caseAdjaClickable()
        For Each unit As [Case] In map.CaseClickable ' Pour chaque case adjacente si elle n'a pas l'opération click alors on l'ajoute
            Dim pic As PictureBox = pnl_ilot.Controls(String.Format("pic_{0}_{1}", unit.Column, unit.Line))
            If (pic.Tag <> "addHandler") Then
                pic.Tag = "addHandler"
                AddHandler pic.Click, AddressOf pic_click
            End If
        Next
    End Sub
    Private Sub coloreCases()
        For Each pic As PictureBox In pnl_ilot.Controls ' Pour chaque case si elle est clickable 
            If (pic.Tag = "addHandler") Then
                Dim id_column, id_line As Integer
                findIDColumnLineFromPic(pic, id_column, id_line)
                Dim unit = map.MapGrid.Item(id_line).Item(id_column)
                If map.ParcelToAdd.Unites.Contains(unit) Then ' Si elle appartient a la parcelle a ajouté
                    pic.BackColor = Color.Gold
                ElseIf map.CaseClickable.Contains(unit) Then ' Si elle est adjacente à la parcelle
                    pic.BackColor = Color.Red
                ElseIf Not pic.BackColor = Color.Silver Then ' Sinon elle ne doit plus etre clickable 
                    pic.BackColor = Color.Transparent
                    disableClickable(pic)
                End If
            End If
        Next
    End Sub
    Private Sub disableClickable(pic As PictureBox)
        pic.Tag = ""
        RemoveHandler pic.Click, AddressOf pic_click
    End Sub
    Private Sub editCaseCliquable(nb)
        If nb = 0 Then ' Si 0 case selectionnée alors tous le plateau est cliquable sauf parcelle deja ajoutée
            plateauClickable()
        Else
            caseAdjaClickable()
            coloreCases()
        End If
    End Sub
    Private Sub btn_back_Click(sender As Object, e As EventArgs) Handles btn_back.Click
        If (map.ParcelToAdd.Unites.Count <> 0) Then
            MsgBox("Veuillez désélectionner les cases de couleur Or")
        Else
            lb_parcel.Visible = True
            btn_creation.Visible = True
            btn_save.Visible = False
            btn_back.Visible = False
            lbl_creation.Visible = False
            map.ParcelToAdd = Nothing
            For Each pic As PictureBox In pnl_ilot.Controls
                If (pic.BackColor = Color.Transparent) Then
                    disableClickable(pic)
                End If
            Next
        End If
    End Sub
    Private Sub colorSavedCases()
        For Each line As List(Of [Case]) In map.MapGrid
            For Each unit As [Case] In line
                Dim pic As PictureBox = pnl_ilot.Controls(String.Format("pic_{0}_{1}", unit.Column, unit.Line))
                If map.CaseClickable.Contains(unit) Then
                    pic.BackColor = Color.Transparent
                ElseIf map.ParcelToAdd.Unites.Contains(unit) Then
                    pic.BackColor = Color.Silver
                    pic.BorderStyle = BorderStyle.None
                End If
            Next
        Next
    End Sub
    Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        Dim nb_case = lb_parcel.SelectedItem.ToString.Split(" ")(2)
        If (nb_case <> map.ParcelToAdd.Unites.Count) Then
            Dim msg As String = IIf(map.ParcelToAdd.Unites.Count <> 1, "{0} cases sont selectionnées, veuillez en selectionner uniquement {1}", "{0} case est selectionnée, veuillez en selectionner uniquement {1}")
            MsgBox(String.Format(msg, map.ParcelToAdd.Unites.Count, nb_case), vbOK, "Erreur")
        Else ' Verifier si les cases sont adjacentes + ajouter a map.IlotParcel + calculer l'équivalent en bytes
            If map.VerifParcelToAdd() Then
                For Each pic As PictureBox In pnl_ilot.Controls
                    If (pic.Tag = "addHandler") Then
                        disableClickable(pic)
                    End If
                Next
                savingClick(nb_case)
            Else
                MsgBox("Veuillez faire en sorte que vos cases soit adjacentes les une aux autres", vbOK, "Erreur")
            End If
        End If
    End Sub

    Private Sub savingClick(nb_case)
        map.CalcCodedValue()
        colorSavedCases()
        ajoutIlotParcel()
        btn_back.Visible = False
        btn_save.Visible = False
        lbl_creation.Visible = False
        map.NbParcel.Item(nb_case) -= 1
        editLbParcel()
        If (lb_parcel.Items.Count <> 0) Then
            lb_parcel.Visible = True
            btn_creation.Visible = True
        Else
            pnl_choix.Visible = True
        End If
    End Sub
    Private Sub ajoutIlotParcel()
        map.Parcelles.Add(map.ParcelToAdd)
        map.CaseClickable = Nothing
        For Each unit In map.ParcelToAdd.Unites
            Dim pic As PictureBox = pnl_ilot.Controls(String.Format("pic_{0}_{1}", unit.Column, unit.Line))
            plateauJeu.createBorderCase(unit, pic)
        Next
        map.ParcelToAdd = Nothing
    End Sub

    Private Sub btn_allMerOrForet_Click(sender As Object, e As EventArgs) Handles btn_allmer.Click, btn_allforet.Click
        Dim btn As Button = sender
        Dim nbToAdd As Int16 = 32
        map.ParcelToAdd = New Parcelle()
        If btn.Text = "Mer" Then
            nbToAdd = 64
        End If
        colorationMerForet(btn, nbToAdd)
        map.CalcCodedValue()
        ajoutIlotParcel()

        lbl_nameCarte.Visible = True
        txt_nameCarte.Visible = True
        txt_nameCarte.Text = ""
        btn_nameCarte.Visible = True
        pnl_choix.Visible = False
    End Sub

    Private Sub colorationMerForet(btn, nbToAdd)
        For Each pic As PictureBox In pnl_ilot.Controls
            If (pic.BackColor = Color.Transparent) Then
                pic.BackColor = IIf(btn.Text = "Mer", Color.Blue, Color.Green)
                pic.BorderStyle = BorderStyle.None
                Dim id_column, id_line As Integer
                findIDColumnLineFromPic(pic, id_column, id_line)
                Dim unit = map.MapGrid.Item(id_line).Item(id_column)
                map.ParcelToAdd.Unites.Add(unit)
            End If
        Next
        For Each unit As [Case] In map.ParcelToAdd.Unites
            unit.Value += nbToAdd
        Next
    End Sub

    Private Sub btn_manuel_Click(sender As Object, e As EventArgs) Handles btn_manuel.Click
        pnl_choix.Visible = False
        pnl_merforet.Visible = True
    End Sub
    Private Sub pic_clickMerForet(sender As Object, e As EventArgs)
        Dim pic As PictureBox = sender

        ajoutRetraitCase(pic)
        If (map.ParcelToAdd.Unites.Count <> 0) Then
            btn_enregistremerforet.Visible = True
        Else
            btn_enregistremerforet.Visible = False
        End If
    End Sub

    Private Sub ajoutRetraitCase(pic)
        Dim id_column, id_line As Integer
        findIDColumnLineFromPic(pic, id_column, id_line)
        Dim unit = map.MapGrid.Item(id_line).Item(id_column)
        If (map.ParcelToAdd.Unites.Contains(unit)) Then
            map.ParcelToAdd.Unites.Remove(unit)
            unit.Parcel = Nothing
            pic.BackColor = Color.Transparent
            pic.BorderStyle = BorderStyle.FixedSingle
        Else
            map.ParcelToAdd.Unites.Add(unit)
            unit.Parcel = map.ParcelToAdd
            pic.BackColor = IIf(rb_mer.Checked, Color.Blue, Color.Green)
            pic.BorderStyle = BorderStyle.None
        End If
    End Sub

    Private Sub btn_createmerforet_Click(sender As Object, e As EventArgs) Handles btn_createmerforet.Click
        lbl_createmerforet2.Text = String.Format("de type {0} avant d'enregistrer", IIf(rb_mer.Checked, "Mer", "Foret"))
        lbl_createmerforet.Visible = True
        lbl_createmerforet2.Visible = True
        btn_createmerforet.Visible = False
        rb_foret.Visible = False
        rb_mer.Visible = False
        For Each pic As PictureBox In pnl_ilot.Controls
            pic.Tag = ""
            RemoveHandler pic.Click, AddressOf pic_click
            RemoveHandler pic.Click, AddressOf pic_clickMerForet
        Next
        For Each pic As PictureBox In pnl_ilot.Controls
            If pic.Tag = "" And pic.BackColor = Color.Transparent Then
                pic.Tag = "addHandler"
                AddHandler pic.Click, AddressOf pic_clickMerForet
            End If
        Next
        map.ParcelToAdd = New Parcelle()
    End Sub
    Private Sub actualisationPanelMerForet()
        lbl_createmerforet2.Visible = False
        lbl_createmerforet.Visible = False
        btn_enregistremerforet.Visible = False
        rb_mer.Visible = True
        rb_foret.Visible = True
        btn_createmerforet.Visible = True
        pnl_merforet.Visible = False
    End Sub
    Private Sub btn_enregistremerforet_Click(sender As Object, e As EventArgs) Handles btn_enregistremerforet.Click
        actualisationPanelMerForet()
        Dim nbToAdd As Int16 = IIf(rb_foret.Checked, 32, 64)
        For Each unit As [Case] In map.ParcelToAdd.Unites
            unit.Value += nbToAdd
        Next
        map.CalcCodedValue()
        ajoutIlotParcel()
        map.ParcelToAdd = Nothing
        For Each pic As PictureBox In pnl_ilot.Controls
            If (pic.Tag = "addHandler" And Not pic.BackColor = Color.Transparent) Then
                pic.Tag = ""
                RemoveHandler pic.Click, AddressOf pic_clickMerForet
            End If
        Next
        verifCarteComplete()
    End Sub
    Private Sub verifCarteComplete()
        Dim mem = 0
        For Each pict As PictureBox In pnl_ilot.Controls
            If (pict.BackColor = Color.Transparent) Then
                mem += 1
            End If
        Next
        If (mem = 0) Then
            lbl_nameCarte.Visible = True
            txt_nameCarte.Visible = True
            txt_nameCarte.Text = ""
            btn_nameCarte.Visible = True
        Else
            pnl_merforet.Visible = True
        End If
    End Sub
    '!INTERDIRE LES CARACTERES % <       
    Private Function nameCarte()
        Dim Path As String = "..\..\..\Cartes.txt"
        Dim b(1023) As Byte
        Dim temp As String = ""
        Dim flag As Boolean = False
        Using fs = File.Open(Path, FileMode.OpenOrCreate)
            Do While fs.Read(b, 0, b.Length) > 0
                temp = String.Format("{0}{1}", temp, Encoding.ASCII.GetString(b))
            Loop
            Dim titleCarte = temp.Split("<")
            For Each str As String In titleCarte
                If str.Split("%")(0) = txt_nameCarte.Text Then
                    flag = True
                End If
            Next
            If flag Then
                MsgBox("Le nom de cette carte est déja prise, veuillez en saisir un autre", vbOK, "Nom déja existant")
            Else
                Dim formatageCarte = String.Format("{0}%{1}<", txt_nameCarte.Text, map.Sentence)
                fs.Write(Encoding.ASCII.GetBytes(formatageCarte), 0, Encoding.ASCII.GetBytes(formatageCarte).Length)
            End If
        End Using
        Return flag
    End Function
    Private Sub btn_nameCarte_Click(sender As Object, e As EventArgs) Handles btn_nameCarte.Click
        If txt_nameCarte.Text = "" Then
            MsgBox("Veuillez saisir un nom pour votre carte", vbOK, "Saisi nom")
        ElseIf Not nameCarte() Then
            accueil.lbl_cartecree.Visible = True
            accueil.addCarte(txt_nameCarte.Text)
            accueil.Show()
            Me.Close()
        End If
    End Sub
End Class