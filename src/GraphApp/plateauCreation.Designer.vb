﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class plateauCreation
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnl_sys = New System.Windows.Forms.Panel()
        Me.btn_nameCarte = New System.Windows.Forms.Button()
        Me.lbl_nameCarte = New System.Windows.Forms.Label()
        Me.txt_nameCarte = New System.Windows.Forms.TextBox()
        Me.pnl_merforet = New System.Windows.Forms.Panel()
        Me.lbl_createmerforet2 = New System.Windows.Forms.Label()
        Me.lbl_createmerforet = New System.Windows.Forms.Label()
        Me.btn_enregistremerforet = New System.Windows.Forms.Button()
        Me.rb_foret = New System.Windows.Forms.RadioButton()
        Me.btn_createmerforet = New System.Windows.Forms.Button()
        Me.rb_mer = New System.Windows.Forms.RadioButton()
        Me.pnl_choix = New System.Windows.Forms.Panel()
        Me.btn_manuel = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_allforet = New System.Windows.Forms.Button()
        Me.lbl_choix = New System.Windows.Forms.Label()
        Me.btn_allmer = New System.Windows.Forms.Button()
        Me.lbl_creation = New System.Windows.Forms.Label()
        Me.btn_back = New System.Windows.Forms.Button()
        Me.btn_save = New System.Windows.Forms.Button()
        Me.btn_creation = New System.Windows.Forms.Button()
        Me.lb_parcel = New System.Windows.Forms.ListBox()
        Me.pnl_ilot = New System.Windows.Forms.Panel()
        Me.pnl_sys.SuspendLayout()
        Me.pnl_merforet.SuspendLayout()
        Me.pnl_choix.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnl_sys
        '
        Me.pnl_sys.Controls.Add(Me.btn_nameCarte)
        Me.pnl_sys.Controls.Add(Me.lbl_nameCarte)
        Me.pnl_sys.Controls.Add(Me.txt_nameCarte)
        Me.pnl_sys.Controls.Add(Me.pnl_merforet)
        Me.pnl_sys.Controls.Add(Me.pnl_choix)
        Me.pnl_sys.Controls.Add(Me.lbl_creation)
        Me.pnl_sys.Controls.Add(Me.btn_back)
        Me.pnl_sys.Controls.Add(Me.btn_save)
        Me.pnl_sys.Controls.Add(Me.btn_creation)
        Me.pnl_sys.Controls.Add(Me.lb_parcel)
        Me.pnl_sys.Location = New System.Drawing.Point(12, 30)
        Me.pnl_sys.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pnl_sys.Name = "pnl_sys"
        Me.pnl_sys.Size = New System.Drawing.Size(248, 555)
        Me.pnl_sys.TabIndex = 1
        '
        'btn_nameCarte
        '
        Me.btn_nameCarte.Location = New System.Drawing.Point(76, 511)
        Me.btn_nameCarte.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_nameCarte.Name = "btn_nameCarte"
        Me.btn_nameCarte.Size = New System.Drawing.Size(75, 23)
        Me.btn_nameCarte.TabIndex = 25
        Me.btn_nameCarte.Text = "Valider"
        Me.btn_nameCarte.UseVisualStyleBackColor = True
        Me.btn_nameCarte.Visible = False
        '
        'lbl_nameCarte
        '
        Me.lbl_nameCarte.AutoSize = True
        Me.lbl_nameCarte.Location = New System.Drawing.Point(3, 454)
        Me.lbl_nameCarte.Name = "lbl_nameCarte"
        Me.lbl_nameCarte.Size = New System.Drawing.Size(240, 17)
        Me.lbl_nameCarte.TabIndex = 24
        Me.lbl_nameCarte.Text = "Veuillez saisir le nom de votre carte :"
        Me.lbl_nameCarte.Visible = False
        '
        'txt_nameCarte
        '
        Me.txt_nameCarte.Location = New System.Drawing.Point(33, 474)
        Me.txt_nameCarte.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_nameCarte.Name = "txt_nameCarte"
        Me.txt_nameCarte.Size = New System.Drawing.Size(167, 22)
        Me.txt_nameCarte.TabIndex = 23
        Me.txt_nameCarte.Visible = False
        '
        'pnl_merforet
        '
        Me.pnl_merforet.Controls.Add(Me.lbl_createmerforet2)
        Me.pnl_merforet.Controls.Add(Me.lbl_createmerforet)
        Me.pnl_merforet.Controls.Add(Me.btn_enregistremerforet)
        Me.pnl_merforet.Controls.Add(Me.rb_foret)
        Me.pnl_merforet.Controls.Add(Me.btn_createmerforet)
        Me.pnl_merforet.Controls.Add(Me.rb_mer)
        Me.pnl_merforet.Location = New System.Drawing.Point(7, 289)
        Me.pnl_merforet.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pnl_merforet.Name = "pnl_merforet"
        Me.pnl_merforet.Size = New System.Drawing.Size(237, 142)
        Me.pnl_merforet.TabIndex = 22
        Me.pnl_merforet.Visible = False
        '
        'lbl_createmerforet2
        '
        Me.lbl_createmerforet2.AutoSize = True
        Me.lbl_createmerforet2.Location = New System.Drawing.Point(7, 25)
        Me.lbl_createmerforet2.Name = "lbl_createmerforet2"
        Me.lbl_createmerforet2.Size = New System.Drawing.Size(200, 17)
        Me.lbl_createmerforet2.TabIndex = 21
        Me.lbl_createmerforet2.Text = "de type {0} avant d'enregistrer"
        Me.lbl_createmerforet2.Visible = False
        '
        'lbl_createmerforet
        '
        Me.lbl_createmerforet.AutoSize = True
        Me.lbl_createmerforet.Location = New System.Drawing.Point(7, 4)
        Me.lbl_createmerforet.Name = "lbl_createmerforet"
        Me.lbl_createmerforet.Size = New System.Drawing.Size(219, 17)
        Me.lbl_createmerforet.TabIndex = 20
        Me.lbl_createmerforet.Text = "Veuillez déposer toutes les cases"
        Me.lbl_createmerforet.Visible = False
        '
        'btn_enregistremerforet
        '
        Me.btn_enregistremerforet.Location = New System.Drawing.Point(115, 98)
        Me.btn_enregistremerforet.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_enregistremerforet.Name = "btn_enregistremerforet"
        Me.btn_enregistremerforet.Size = New System.Drawing.Size(92, 32)
        Me.btn_enregistremerforet.TabIndex = 19
        Me.btn_enregistremerforet.Text = "Enregistrer"
        Me.btn_enregistremerforet.UseVisualStyleBackColor = True
        Me.btn_enregistremerforet.Visible = False
        '
        'rb_foret
        '
        Me.rb_foret.AutoSize = True
        Me.rb_foret.Location = New System.Drawing.Point(61, 71)
        Me.rb_foret.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.rb_foret.Name = "rb_foret"
        Me.rb_foret.Size = New System.Drawing.Size(62, 21)
        Me.rb_foret.TabIndex = 18
        Me.rb_foret.TabStop = True
        Me.rb_foret.Text = "Foret"
        Me.rb_foret.UseVisualStyleBackColor = True
        '
        'btn_createmerforet
        '
        Me.btn_createmerforet.Location = New System.Drawing.Point(28, 98)
        Me.btn_createmerforet.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_createmerforet.Name = "btn_createmerforet"
        Me.btn_createmerforet.Size = New System.Drawing.Size(75, 32)
        Me.btn_createmerforet.TabIndex = 16
        Me.btn_createmerforet.Text = "Création"
        Me.btn_createmerforet.UseVisualStyleBackColor = True
        '
        'rb_mer
        '
        Me.rb_mer.AutoSize = True
        Me.rb_mer.Location = New System.Drawing.Point(61, 47)
        Me.rb_mer.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.rb_mer.Name = "rb_mer"
        Me.rb_mer.Size = New System.Drawing.Size(53, 21)
        Me.rb_mer.TabIndex = 17
        Me.rb_mer.TabStop = True
        Me.rb_mer.Text = "Mer"
        Me.rb_mer.UseVisualStyleBackColor = True
        '
        'pnl_choix
        '
        Me.pnl_choix.Controls.Add(Me.btn_manuel)
        Me.pnl_choix.Controls.Add(Me.Label1)
        Me.pnl_choix.Controls.Add(Me.btn_allforet)
        Me.pnl_choix.Controls.Add(Me.lbl_choix)
        Me.pnl_choix.Controls.Add(Me.btn_allmer)
        Me.pnl_choix.Location = New System.Drawing.Point(3, 183)
        Me.pnl_choix.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.pnl_choix.Name = "pnl_choix"
        Me.pnl_choix.Size = New System.Drawing.Size(239, 100)
        Me.pnl_choix.TabIndex = 21
        Me.pnl_choix.Visible = False
        '
        'btn_manuel
        '
        Me.btn_manuel.Location = New System.Drawing.Point(73, 75)
        Me.btn_manuel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_manuel.Name = "btn_manuel"
        Me.btn_manuel.Size = New System.Drawing.Size(75, 23)
        Me.btn_manuel.TabIndex = 6
        Me.btn_manuel.Text = "Manuel"
        Me.btn_manuel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(208, 17)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Ou sélectionner manuellement :"
        '
        'btn_allforet
        '
        Me.btn_allforet.Location = New System.Drawing.Point(123, 25)
        Me.btn_allforet.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_allforet.Name = "btn_allforet"
        Me.btn_allforet.Size = New System.Drawing.Size(75, 23)
        Me.btn_allforet.TabIndex = 4
        Me.btn_allforet.Text = "Foret"
        Me.btn_allforet.UseVisualStyleBackColor = True
        '
        'lbl_choix
        '
        Me.lbl_choix.AutoSize = True
        Me.lbl_choix.Location = New System.Drawing.Point(4, 4)
        Me.lbl_choix.Name = "lbl_choix"
        Me.lbl_choix.Size = New System.Drawing.Size(232, 17)
        Me.lbl_choix.TabIndex = 3
        Me.lbl_choix.Text = "Definir les cases restantes comme :"
        Me.lbl_choix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_allmer
        '
        Me.btn_allmer.Location = New System.Drawing.Point(29, 25)
        Me.btn_allmer.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_allmer.Name = "btn_allmer"
        Me.btn_allmer.Size = New System.Drawing.Size(75, 23)
        Me.btn_allmer.TabIndex = 2
        Me.btn_allmer.Text = "Mer"
        Me.btn_allmer.UseVisualStyleBackColor = True
        '
        'lbl_creation
        '
        Me.lbl_creation.AutoSize = True
        Me.lbl_creation.Location = New System.Drawing.Point(13, 50)
        Me.lbl_creation.Name = "lbl_creation"
        Me.lbl_creation.Size = New System.Drawing.Size(93, 17)
        Me.lbl_creation.TabIndex = 4
        Me.lbl_creation.Text = "label creation"
        Me.lbl_creation.Visible = False
        '
        'btn_back
        '
        Me.btn_back.Location = New System.Drawing.Point(16, 145)
        Me.btn_back.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_back.Name = "btn_back"
        Me.btn_back.Size = New System.Drawing.Size(97, 32)
        Me.btn_back.TabIndex = 3
        Me.btn_back.Text = "Retour"
        Me.btn_back.UseVisualStyleBackColor = True
        Me.btn_back.Visible = False
        '
        'btn_save
        '
        Me.btn_save.Location = New System.Drawing.Point(135, 145)
        Me.btn_save.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Size = New System.Drawing.Size(97, 32)
        Me.btn_save.TabIndex = 2
        Me.btn_save.Text = "Enregistrer"
        Me.btn_save.UseVisualStyleBackColor = True
        Me.btn_save.Visible = False
        '
        'btn_creation
        '
        Me.btn_creation.Location = New System.Drawing.Point(77, 145)
        Me.btn_creation.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_creation.Name = "btn_creation"
        Me.btn_creation.Size = New System.Drawing.Size(75, 23)
        Me.btn_creation.TabIndex = 1
        Me.btn_creation.Text = "Création"
        Me.btn_creation.UseVisualStyleBackColor = True
        '
        'lb_parcel
        '
        Me.lb_parcel.FormattingEnabled = True
        Me.lb_parcel.ItemHeight = 16
        Me.lb_parcel.Location = New System.Drawing.Point(16, 14)
        Me.lb_parcel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.lb_parcel.Name = "lb_parcel"
        Me.lb_parcel.Size = New System.Drawing.Size(216, 116)
        Me.lb_parcel.TabIndex = 0
        '
        'pnl_ilot
        '
        Me.pnl_ilot.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.pnl_ilot.Location = New System.Drawing.Point(372, 14)
        Me.pnl_ilot.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pnl_ilot.Name = "pnl_ilot"
        Me.pnl_ilot.Size = New System.Drawing.Size(1000, 923)
        Me.pnl_ilot.TabIndex = 8
        '
        'plateauCreation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1445, 1006)
        Me.Controls.Add(Me.pnl_ilot)
        Me.Controls.Add(Me.pnl_sys)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MinimumSize = New System.Drawing.Size(1461, 980)
        Me.Name = "plateauCreation"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "plateauCreation"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnl_sys.ResumeLayout(False)
        Me.pnl_sys.PerformLayout()
        Me.pnl_merforet.ResumeLayout(False)
        Me.pnl_merforet.PerformLayout()
        Me.pnl_choix.ResumeLayout(False)
        Me.pnl_choix.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnl_sys As Panel
    Friend WithEvents pnl_ilot As Panel
    Friend WithEvents btn_back As Button
    Friend WithEvents btn_save As Button
    Friend WithEvents btn_creation As Button
    Friend WithEvents lb_parcel As ListBox
    Friend WithEvents lbl_creation As Label
    Friend WithEvents pnl_choix As Panel
    Friend WithEvents btn_manuel As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents btn_allforet As Button
    Friend WithEvents lbl_choix As Label
    Friend WithEvents btn_allmer As Button
    Friend WithEvents pnl_merforet As Panel
    Friend WithEvents lbl_createmerforet2 As Label
    Friend WithEvents lbl_createmerforet As Label
    Friend WithEvents btn_enregistremerforet As Button
    Friend WithEvents rb_foret As RadioButton
    Friend WithEvents btn_createmerforet As Button
    Friend WithEvents rb_mer As RadioButton
    Friend WithEvents lbl_nameCarte As Label
    Friend WithEvents txt_nameCarte As TextBox
    Friend WithEvents btn_nameCarte As Button
End Class
