﻿Imports CoffeeBattle
Public Class resultat
    ReadOnly oldCarte = Serveur.Game.Map.Sentence()
    Private Sub resultat_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim formatText = "• {0} est {1} avec {2} points"
        If Serveur.Game.Players(0).ScoreTotal = Serveur.Game.Players(1).ScoreTotal Then
            lbl_premier.Text = String.Format(formatText, Serveur.Game.Players(0).Name, "premier ex-aequo", Serveur.Game.Players(0).ScoreTotal)
            lbl_dernier.Text = String.Format(formatText, Serveur.Game.Players(1).Name, "premier ex-aequo", Serveur.Game.Players(1).ScoreTotal)
        Else
            lbl_premier.Text = String.Format(formatText, Serveur.Game.Players(0).Name, "premier", Serveur.Game.Players(0).ScoreTotal)
            lbl_dernier.Text = String.Format(formatText, Serveur.Game.Players(1).Name, "deuxieme", Serveur.Game.Players(1).ScoreTotal)
        End If


        lbl_premierscore.Text = String.Format("Gain de {0} points avec les parcelles et de {1} points de cases adjacentes", Serveur.Game.Players(0).ScoreParcelle, Serveur.Game.Players(0).ScoreBonus)
        lbl_dernierscore.Text = String.Format("Gain de {0} points avec les parcelles et de {1} points de cases adjacentes", Serveur.Game.Players(1).ScoreParcelle, Serveur.Game.Players(1).ScoreBonus)
        plateauJeu.Close()
    End Sub

    Private Sub btn_accueil_Click(sender As Object, e As EventArgs) Handles btn_accueil.Click
        accueil.Show()
        Me.Close()
    End Sub
End Class