﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class plateauJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.pnl_ilot = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lbl_namecarte = New System.Windows.Forms.Label()
        Me.btn_result = New System.Windows.Forms.Button()
        Me.lbl_pts2 = New System.Windows.Forms.Label()
        Me.lbl_pts1 = New System.Windows.Forms.Label()
        Me.lbl_currentplayer = New System.Windows.Forms.Label()
        Me.lbl_name2 = New System.Windows.Forms.Label()
        Me.lbl_name1 = New System.Windows.Forms.Label()
        Me.lbl_coffee2 = New System.Windows.Forms.Label()
        Me.lbl_coffee1 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btn_valid = New System.Windows.Forms.Button()
        Me.btn_annul = New System.Windows.Forms.Button()
        Me.btn_lancementIa = New System.Windows.Forms.Button()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.btn_nextIa = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnl_ilot
        '
        Me.pnl_ilot.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pnl_ilot.Location = New System.Drawing.Point(332, 15)
        Me.pnl_ilot.Margin = New System.Windows.Forms.Padding(4)
        Me.pnl_ilot.Name = "pnl_ilot"
        Me.pnl_ilot.Size = New System.Drawing.Size(1000, 923)
        Me.pnl_ilot.TabIndex = 7
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lbl_namecarte)
        Me.Panel1.Controls.Add(Me.btn_result)
        Me.Panel1.Controls.Add(Me.lbl_pts2)
        Me.Panel1.Controls.Add(Me.lbl_pts1)
        Me.Panel1.Controls.Add(Me.lbl_currentplayer)
        Me.Panel1.Controls.Add(Me.lbl_name2)
        Me.Panel1.Controls.Add(Me.lbl_name1)
        Me.Panel1.Controls.Add(Me.lbl_coffee2)
        Me.Panel1.Controls.Add(Me.lbl_coffee1)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Location = New System.Drawing.Point(16, 15)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(229, 398)
        Me.Panel1.TabIndex = 8
        '
        'lbl_namecarte
        '
        Me.lbl_namecarte.AutoSize = True
        Me.lbl_namecarte.Location = New System.Drawing.Point(21, 21)
        Me.lbl_namecarte.Name = "lbl_namecarte"
        Me.lbl_namecarte.Size = New System.Drawing.Size(94, 17)
        Me.lbl_namecarte.TabIndex = 17
        Me.lbl_namecarte.Text = "Carte : Scabb"
        '
        'btn_result
        '
        Me.btn_result.Location = New System.Drawing.Point(21, 364)
        Me.btn_result.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btn_result.Name = "btn_result"
        Me.btn_result.Size = New System.Drawing.Size(187, 23)
        Me.btn_result.TabIndex = 16
        Me.btn_result.Text = "Accéder au classement"
        Me.btn_result.UseVisualStyleBackColor = True
        '
        'lbl_pts2
        '
        Me.lbl_pts2.AutoSize = True
        Me.lbl_pts2.Location = New System.Drawing.Point(97, 295)
        Me.lbl_pts2.Name = "lbl_pts2"
        Me.lbl_pts2.Size = New System.Drawing.Size(39, 17)
        Me.lbl_pts2.TabIndex = 15
        Me.lbl_pts2.Text = "0 pts"
        '
        'lbl_pts1
        '
        Me.lbl_pts1.AutoSize = True
        Me.lbl_pts1.Location = New System.Drawing.Point(97, 140)
        Me.lbl_pts1.Name = "lbl_pts1"
        Me.lbl_pts1.Size = New System.Drawing.Size(39, 17)
        Me.lbl_pts1.TabIndex = 14
        Me.lbl_pts1.Text = "0 pts"
        '
        'lbl_currentplayer
        '
        Me.lbl_currentplayer.AutoSize = True
        Me.lbl_currentplayer.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_currentplayer.Location = New System.Drawing.Point(17, 66)
        Me.lbl_currentplayer.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_currentplayer.Name = "lbl_currentplayer"
        Me.lbl_currentplayer.Size = New System.Drawing.Size(143, 19)
        Me.lbl_currentplayer.TabIndex = 13
        Me.lbl_currentplayer.Text = "Tour de Joueur 1"
        '
        'lbl_name2
        '
        Me.lbl_name2.AutoSize = True
        Me.lbl_name2.Location = New System.Drawing.Point(97, 268)
        Me.lbl_name2.Name = "lbl_name2"
        Me.lbl_name2.Size = New System.Drawing.Size(64, 17)
        Me.lbl_name2.TabIndex = 12
        Me.lbl_name2.Text = "Joueur 2"
        '
        'lbl_name1
        '
        Me.lbl_name1.AutoSize = True
        Me.lbl_name1.Location = New System.Drawing.Point(97, 111)
        Me.lbl_name1.Name = "lbl_name1"
        Me.lbl_name1.Size = New System.Drawing.Size(64, 17)
        Me.lbl_name1.TabIndex = 11
        Me.lbl_name1.Text = "Joueur 1"
        '
        'lbl_coffee2
        '
        Me.lbl_coffee2.AutoSize = True
        Me.lbl_coffee2.Location = New System.Drawing.Point(97, 324)
        Me.lbl_coffee2.Name = "lbl_coffee2"
        Me.lbl_coffee2.Size = New System.Drawing.Size(87, 17)
        Me.lbl_coffee2.TabIndex = 10
        Me.lbl_coffee2.Text = "28 restantes"
        '
        'lbl_coffee1
        '
        Me.lbl_coffee1.AutoSize = True
        Me.lbl_coffee1.Location = New System.Drawing.Point(95, 167)
        Me.lbl_coffee1.Name = "lbl_coffee1"
        Me.lbl_coffee1.Size = New System.Drawing.Size(87, 17)
        Me.lbl_coffee1.TabIndex = 9
        Me.lbl_coffee1.Text = "28 restantes"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = Global.GraphApp.My.Resources.Resources.coffee2
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox2.Location = New System.Drawing.Point(16, 266)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(75, 75)
        Me.PictureBox2.TabIndex = 8
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.GraphApp.My.Resources.Resources.coffee1
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox1.Location = New System.Drawing.Point(16, 111)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(75, 75)
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        '
        'btn_valid
        '
        Me.btn_valid.Location = New System.Drawing.Point(145, 642)
        Me.btn_valid.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_valid.Name = "btn_valid"
        Me.btn_valid.Size = New System.Drawing.Size(100, 28)
        Me.btn_valid.TabIndex = 9
        Me.btn_valid.Text = "Valider"
        Me.btn_valid.UseVisualStyleBackColor = True
        Me.btn_valid.Visible = False
        '
        'btn_annul
        '
        Me.btn_annul.Location = New System.Drawing.Point(41, 642)
        Me.btn_annul.Margin = New System.Windows.Forms.Padding(4)
        Me.btn_annul.Name = "btn_annul"
        Me.btn_annul.Size = New System.Drawing.Size(100, 28)
        Me.btn_annul.TabIndex = 10
        Me.btn_annul.Text = "Annuler"
        Me.btn_annul.UseVisualStyleBackColor = True
        Me.btn_annul.Visible = False
        '
        'btn_lancementIa
        '
        Me.btn_lancementIa.Location = New System.Drawing.Point(85, 513)
        Me.btn_lancementIa.Name = "btn_lancementIa"
        Me.btn_lancementIa.Size = New System.Drawing.Size(139, 23)
        Me.btn_lancementIa.TabIndex = 11
        Me.btn_lancementIa.Text = "Lancement Partie"
        Me.btn_lancementIa.UseVisualStyleBackColor = True
        Me.btn_lancementIa.Visible = False
        '
        'btn_nextIa
        '
        Me.btn_nextIa.Location = New System.Drawing.Point(85, 552)
        Me.btn_nextIa.Name = "btn_nextIa"
        Me.btn_nextIa.Size = New System.Drawing.Size(139, 23)
        Me.btn_nextIa.TabIndex = 12
        Me.btn_nextIa.Text = "Prochain Coup"
        Me.btn_nextIa.UseVisualStyleBackColor = True
        Me.btn_nextIa.Visible = False
        '
        'plateauJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1579, 881)
        Me.Controls.Add(Me.btn_nextIa)
        Me.Controls.Add(Me.btn_lancementIa)
        Me.Controls.Add(Me.btn_annul)
        Me.Controls.Add(Me.btn_valid)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnl_ilot)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MinimumSize = New System.Drawing.Size(1461, 856)
        Me.Name = "plateauJeu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Plateau"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnl_ilot As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lbl_name2 As Label
    Friend WithEvents lbl_name1 As Label
    Friend WithEvents lbl_coffee2 As Label
    Friend WithEvents lbl_coffee1 As Label
    Friend WithEvents lbl_currentplayer As Label
    Friend WithEvents lbl_pts2 As Label
    Friend WithEvents lbl_pts1 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents btn_result As Button
    Friend WithEvents lbl_namecarte As Label
    Friend WithEvents btn_valid As Button
    Friend WithEvents btn_annul As Button
    Friend WithEvents btn_lancementIa As Button
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btn_nextIa As Button
End Class
