﻿Imports System.Net.Sockets
Imports System.Text
Imports CoffeeBattle

Public Class plateauJeu
    ReadOnly sizePB As Double = 75.0
    Dim unitTemp As [Case]
    Dim picTemp As PictureBox
    Dim oldUnit As [Case]
    Private Sub Plateau_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        affichageStart()
        For Each line As List(Of [Case]) In Serveur.Game.Map.MapGrid
            For Each unit As [Case] In line
                Dim pic As PictureBox = createPictureBox(unit)
                createBorderCase(unit, pic)
            Next
        Next
        accueil.Close()
        If Serveur.Game.GameMode = ModeDeJeu.IAVsIA Then
            btn_lancementIa.Visible = True
            annulClickable()
        End If
    End Sub

    Private Sub annulClickable()
        For Each pic As PictureBox In pnl_ilot.Controls
            RemoveHandler pic.Click, AddressOf Pic_Click
        Next
    End Sub

    Private Sub affichageStart()
        If Serveur.Game.GameMode = ModeDeJeu.JoueurVsJoueur Then
            lbl_namecarte.Text = String.Format("Carte : {0}", Serveur.Game.Map.MapName)
        Else
            lbl_namecarte.Visible = False
        End If
        lbl_name1.Text = Serveur.Game.Players(0).Name
        lbl_name2.Text = Serveur.Game.Players(1).Name
        Dim currentplayer As Joueur = getCurrentPlayer()
        lbl_currentplayer.Text = String.Format("Tour N°1 : {0}", currentplayer.Name)
        btn_result.Visible = False
        Dim sentence_receive As Byte() = New Byte() {}
        pnl_ilot.Height = sizePB * 10
        pnl_ilot.Width = sizePB * 10
    End Sub
    Private Function createPictureBox(unit)
        Dim pic As New PictureBox With {
            .Size = New Size(New Point(sizePB, sizePB)),
            .Name = String.Format("pic_{0}_{1}", unit.Column, unit.Line)
        }
        If unit.Type = TypeCases.Forest Then
            pic.BackgroundImage = My.Resources.foret
            pic.BackgroundImageLayout = ImageLayout.Stretch
        ElseIf unit.Type = TypeCases.Water Then
            pic.BackgroundImage = My.Resources.mer
            pic.BackgroundImageLayout = ImageLayout.Stretch
        Else
            pic.BackColor = Color.Silver
            AddHandler pic.Click, AddressOf Pic_Click
        End If
        pnl_ilot.Controls.Add(pic)
        pic.BorderStyle = BorderStyle.None
        pic.Location = New Point(unit.Column * sizePB, unit.Line * sizePB)
        Return pic
    End Function

    Public Sub createBorderCase(unit, pic)
        Dim allBorder As New List(Of BorderCases)
        allBorder.AddRange([Enum].GetValues(GetType(BorderCases)).Cast(Of BorderCases))
        allBorder.Remove(BorderCases.Unknown)
        While allBorder.Count > 0
            firstBorder(unit, pic, allBorder.Item(0))
            allBorder.Remove(allBorder.Item(0))
        End While
    End Sub

    Private Sub firstBorder(unit, pic, bordure)
        Dim border As New PictureBox With {
            .Location = New Point(0, 0),
            .BackColor = Color.Black
        }
        If (unit.Borders.Count > 0 And unit.Borders.Contains(bordure) Or unit.Type = TypeCases.Available) Then
            placementBordure(bordure, IIf(unit.Borders.Count > 0 And unit.Borders.Contains(bordure), 1, 0), border)
            pic.Controls.Add(border)
        End If
    End Sub

    Private Sub placementBordure(bordure, ind_size, border)
        Dim size As Double() = New Double() {1.0, 3}
        If bordure = BorderCases.North Or bordure = BorderCases.South Then
            border.Width = sizePB
            border.Height = size(ind_size)
            If bordure = BorderCases.South Then
                border.Location = New Point(0, sizePB - size(ind_size))
            End If
        Else
            border.Width = size(ind_size)
            border.Height = sizePB
            If bordure = BorderCases.East Then
                border.Location = New Point(sizePB - size(ind_size), 0)
            End If
        End If
    End Sub

    Private Sub editScore()
        Dim Players As List(Of Joueur) = Serveur.Game.Players
        Serveur.Game.Players(0).CountPoints()
        Serveur.Game.Players(1).CountPoints()
        lbl_pts1.Text = String.Format("{0} pts | {1} + {2}", Players(0).ScoreTotal, Players(0).ScoreParcelle, Players(0).ScoreBonus)
        lbl_pts2.Text = String.Format("{0} pts | {1} + {2}", Players(1).ScoreTotal, Players(1).ScoreParcelle, Players(1).ScoreBonus)
    End Sub
    Private Sub Pic_Click(sender As Object, e As EventArgs)
        Dim pic As PictureBox = sender
        If pic.BackgroundImage Is Nothing Then
            Dim id_column, id_line As Integer
            plateauCreation.findIDColumnLineFromPic(pic, id_column, id_line)
            unitTemp = Serveur.Game.Map.MapGrid.Item(id_line).Item(id_column)
            pic.BackColor = Color.Red
            picTemp = pic
            For Each pict As PictureBox In pnl_ilot.Controls
                Try
                    RemoveHandler pict.Click, AddressOf Pic_Click
                Catch ex As Exception
                End Try
            Next

            btn_valid.Visible = True
            btn_annul.Visible = True
        End If
    End Sub

    Private Sub editPanelJoueur(player, pic)
        If (player Is Serveur.Game.Players(0)) Then
            lbl_coffee1.Text = String.Format("{0} graines restantes", player.Graines.Count)
            Try
                pic.BackgroundImage = My.Resources.coffee1
            Catch ex As Exception
            End Try
        Else
            lbl_coffee2.Text = String.Format("{0} graines restantes", player.Graines.Count)
            Try
                pic.BackgroundImage = My.Resources.coffee2
            Catch ex As Exception
            End Try
        End If
        pic.BackgroundImageLayout = ImageLayout.Center
    End Sub

    Private Sub editAllowClick(unit)
        Dim rules = Serveur.Game.CaseJouable(unit)
        If (rules IsNot Nothing) Then
            For Each pict As PictureBox In pnl_ilot.Controls
                pict.BackColor = Color.Silver
                Try
                    RemoveHandler pict.Click, AddressOf Pic_Click
                Catch ex As Exception
                End Try
            Next
            For Each unitBis As [Case] In rules
                Dim pic As PictureBox = pnl_ilot.Controls(String.Format("pic_{0}_{1}", unitBis.Column, unitBis.Line))
                pic.BackColor = Color.LightGray
                AddHandler pic.Click, AddressOf Pic_Click
            Next
        Else
            boxFinal()
        End If
    End Sub

    Private Sub boxFinal()
        Dim Msg, Style, Title, Response
        Msg = "La partie est finie, voulez vous accéder au classement ?"
        Style = vbYesNo + vbDefaultButton1
        Title = "End Game"
        Response = MsgBox(Msg, Style, Title)
        If Response = vbYes Then
            Serveur.Game.SortScorePlayers()
            Serveur.Close()
            resultat.Show()
            Me.Hide()
        Else
            btn_result.Visible = True
            btn_nextIa.Visible = False
            For Each pic As PictureBox In pnl_ilot.Controls
                If pic.BackColor = Color.LightGray Then
                    pic.BackColor = Color.Silver
                End If
                RemoveHandler pic.Click, AddressOf Pic_Click
            Next
        End If
    End Sub
    Public Function getCurrentPlayer()
        Return Serveur.Game.CurrentPlayer
    End Function

    Private Sub btn_result_Click(sender As Object, e As EventArgs) Handles btn_result.Click
        resultat.Show()
        Me.Hide()
    End Sub

    Private Sub btn_valid_Click(sender As Object, e As EventArgs) Handles btn_valid.Click
        Dim player As Joueur = getCurrentPlayer()
        player.PlayGraine(unitTemp)
        editScore()
        editPanelJoueur(player, picTemp)
        btn_annul.Visible = False
        btn_valid.Visible = False
        oldUnit = unitTemp
        editAllowClick(unitTemp)
        If (Serveur.Game.Players(0).Graines.Count = 0 And Serveur.Game.Players(1).Graines.Count = 0) Then
            boxFinal()
        End If
        Serveur.Game.NextRound()

        Dim currentplayer As Joueur = getCurrentPlayer()
        lbl_currentplayer.Text = String.Format("Tour N°{0} de {1}", Serveur.Game.Tour, currentplayer.Name)
        ' Envoie de la case + prochain tour
        If (Serveur.Game.GameMode = ModeDeJeu.JoueurVsIAExt) Then
            Dim result As String = currentplayer.GetIaRemotePlay()
            Dim resultSplited = result.Split(":")
            picTemp = pnl_ilot.Controls(String.Format("pic_{0}_{1}", resultSplited(1)(1), resultSplited(1)(0)))
            editPanelJoueur(currentplayer, picTemp)
            editScore()
            If (result(0) = "E") Then
                Dim id_column, id_line As Integer
                plateauCreation.findIDColumnLineFromPic(picTemp, id_column, id_line)
                unitTemp = Serveur.Game.Map.MapGrid.Item(id_line).Item(id_column)
                editAllowClick(unitTemp)
                oldUnit = unitTemp
                Serveur.Game.NextRound()
            Else
                boxFinal()
            End If
        ElseIf (Serveur.Game.GameMode = ModeDeJeu.JoueurVsIAInt) Then
            Dim Ia As IA = getCurrentPlayer()
            Ia.PlayGraine(Ia.ResearchBestPlacement(oldUnit))

            picTemp = pnl_ilot.Controls(String.Format("pic_{0}_{1}", Ia.UnitPlay.Column, Ia.UnitPlay.Line))

            editScore()
            editPanelJoueur(Ia, picTemp)
            playIaExt()
            Dim id_column, id_line As Integer
            plateauCreation.findIDColumnLineFromPic(picTemp, id_column, id_line)
            unitTemp = Serveur.Game.Map.MapGrid.Item(id_line).Item(id_column)
            editAllowClick(unitTemp)
        End If
    End Sub

    Private Sub Btn_annul_Click(sender As Object, e As EventArgs) Handles btn_annul.Click
        ' Enlever la case / points ...
        picTemp.BackColor = Color.Silver
        Try
            editAllowClick(oldUnit)
        Catch ex As Exception
            For Each pict As PictureBox In pnl_ilot.Controls
                AddHandler pict.Click, AddressOf Pic_Click
            Next
        End Try
        btn_annul.Visible = False
        btn_valid.Visible = False
    End Sub

    Private Sub btn_lancementIa_Click(sender As Object, e As EventArgs) Handles btn_lancementIa.Click
        Dim ia As IA = Serveur.Game.Players(0)
        Dim unit As [Case] = ia.FirstPlacement()
        ia.PlayGraine(unit)
        editScore()
        Dim pic As PictureBox = pnl_ilot.Controls(String.Format("pic_{0}_{1}", unit.Column, unit.Line))
        editPanelJoueur(ia, pic)

        playIaExt()
        btn_lancementIa.Visible = False
        btn_nextIa.Visible = True
    End Sub

    Private Sub btn_nextIa_Click(sender As Object, e As EventArgs) Handles btn_nextIa.Click
        Dim Ia As IA = getCurrentPlayer()
        Ia.PlayGraine(Ia.ResearchBestPlacement(oldUnit))

        picTemp = pnl_ilot.Controls(String.Format("pic_{0}_{1}", Ia.UnitPlay.Column, Ia.UnitPlay.Line))

        editScore()
        editPanelJoueur(Ia, picTemp)
        playIaExt()
    End Sub

    Private Sub playIaExt()
        Serveur.Game.NextRound()

        Dim currentIa As IA = getCurrentPlayer()
        lbl_currentplayer.Text = String.Format("Tour N°{0} de {1}", Serveur.Game.Tour, currentIa.Name)
        ' Envoie de la case + prochain tour
        If (Serveur.Game.GameMode = ModeDeJeu.IAVsIA) Then
            Dim result As String = currentIa.GetIaRemotePlay()
            If (result Is Nothing) Then
                boxFinal()
            Else
                Dim resultSplited = result.Split(":")
                picTemp = pnl_ilot.Controls(String.Format("pic_{0}_{1}", resultSplited(1)(1), resultSplited(1)(0)))
                editPanelJoueur(currentIa, picTemp)
                editScore()
                Dim id_column, id_line As Integer
                plateauCreation.findIDColumnLineFromPic(picTemp, id_column, id_line)
                oldUnit = Serveur.Game.Map.MapGrid.Item(id_line).Item(id_column)
                Serveur.Game.NextRound()
                If (result(0) = "F") Then
                    boxFinal()
                End If
            End If
        End If
    End Sub
End Class
