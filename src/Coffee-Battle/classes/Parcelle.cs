﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeBattle
{
    public class Parcelle
    {
        private List<Case> unites;
        private Joueur proprio;

        public List<Case> Unites
        {
            get
            {
                return unites;
            }
            set
            {
                if (value != null)
                    unites = value;
            }
        }
        public Joueur Proprio
        {
            get
            {
                return proprio;
            }
            set
            {
                proprio = value;
            }
        }
        public Parcelle(List<Case> unites, Joueur proprio)
        {
            Unites = unites;
            Proprio = proprio;
        }
        public Parcelle()
        {
            Unites = new List<Case>();
            Proprio = null;
        }

        public void EditOwnerParcelle()
        {
            int j1 = 0,j2= 0;
            foreach(Case unit in Unites)
            {
                if(unit.Owner !=null)
                {
                    if (unit.Owner == Serveur.Game.Players[0])
                        j1++;
                    else if (unit.Owner == Serveur.Game.Players[1])
                        j2++;
                }
            }
            Proprio = (j1>j2? Serveur.Game.Players[0] : (j2>j1 ? Serveur.Game.Players[1] : null));
        }
    }
}
