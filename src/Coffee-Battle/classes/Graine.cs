﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeBattle
{
    public class Graine
    {
        private Case emplacement;
        private Joueur owner;
        public const int NB_SEEDS = 28;

        public Joueur Owner
        {
            set
            {
                owner = value;
            }
            get
            {
                return owner;
            }
        }
        public Case Location
        {
            set
            {
                if (value != null)
                    emplacement = value;
            }

            get
            {
                return emplacement;
            }
        }
        public Graine()
        {
            emplacement = null;
            owner = null;
        }
        public Graine(Joueur owner)
        {
            Owner = owner;
            emplacement = null;
        }
    }
}
