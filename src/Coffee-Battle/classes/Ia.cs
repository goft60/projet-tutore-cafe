﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeBattle
{
    public class IA  : Joueur
    {
        private Case unitPlay;

        public Case UnitPlay
        {
            get
            {
                return unitPlay;
            }
            set
            {
                if (value != null)
                    unitPlay = value;
            }
        }

        public IA(string name) : base(name)
        { }
        public Case ResearchBestPlacement(Case unit)
        {
            List<Case> BestsCase = new List<Case>();
            int ScoreInit = ScoreTotal;
            int ParcellesAdjMax = -100;
            foreach (Case placable in Serveur.Game.CaseJouable(unit))
            {
                Map essai = new Map(Serveur.Game.Map);
                int ParcellesAdjHere = ParcelAdj(0, placable);
                if (ParcellesAdjHere == ParcellesAdjMax)
                    BestsCase.Add(placable);
                else if (ParcellesAdjHere > ParcellesAdjMax)
                {
                    ParcellesAdjMax = ParcellesAdjHere;
                    BestsCase = new List<Case>();
                    BestsCase.Add(placable);
                }
            }
            if (BestsCase != null)
            {
                int ScoreMax = -100;
                Case BestCase = null;
                foreach(Case CaseScorable in BestsCase)
                {
                    TestGraine(CaseScorable);
                    CountPoints();
                    Reset(CaseScorable);
                    if (ScoreTotal - ScoreInit >= ScoreMax)
                    {
                        BestCase = CaseScorable;
                        ScoreMax = ScoreTotal - ScoreInit;
                    }
                }
                    return BestCase;
            }
            else
                return Serveur.Game.Map.CaseClickable[0];
        }

        private int ParcelAdj(int ParcellesAdjHere, Case unite)
        {
            // -1 line +1 line -1 col +1 col
            for (int AddHere = 0; AddHere <= 1; AddHere++)
            {
                for (int AddOrSub = -1; AddOrSub <=1 ; AddOrSub += 2)
                {
                    try
                    {
                        Case TestingCase = (AddHere == 0 ? Serveur.Game.Map.MapGrid[unite.Line + AddOrSub][unite.Column] : Serveur.Game.Map.MapGrid[unite.Line][unite.Column + AddOrSub]);
                        // Deuxieme Ia meilleure contre TOUS vise la victoire sinon egalité
                        if (TestingCase.Type == TypeCases.Water || TestingCase.Type == TypeCases.Forest) // Si il y a des cases non jouables proches
                            ParcellesAdjHere -= 10;
                        else
                        {
                            ParcellesAdjHere += (TestingCase.Owner == this ? 20 : (TestingCase.Owner == null ? 5 : 50)); // Définition priorité
                            if (TestingCase.Parcel != unite.Parcel) // Si il y a une parcelle différentes à coté
                                ParcellesAdjHere += TestingCase.Parcel.Unites.Count;
                        }

                        // Premiere Ia
                        /*
                        if (TestingCase.Parcel != unite.Parcel && !(TestingCase.Type == TypeCases.Water || TestingCase.Type == TypeCases.Forest) )
                            ParcellesAdjHere+=5;
                        if ((TestingCase.Owner == null || TestingCase.Owner == this) &&  !(TestingCase.Type == TypeCases.Water || TestingCase.Type == TypeCases.Forest)) 
                            ParcellesAdjHere+=2;
                        if(TestingCase.Type == TypeCases.Water || TestingCase.Type == TypeCases.Forest)
                            ParcellesAdjHere-=10;*/
                    }
                    catch (Exception e) // Si il n'y a pas de case adjacente
                    {
                        ParcellesAdjHere-=10;
                    }
                }
            }
            return ParcellesAdjHere;
        }

        public Case FirstPlacement()
        {
            Case Placement = null;
            int ParcellesAdjMax = -100;
            foreach(Parcelle parcel in Serveur.Game.Map.Parcelles)
            {
                if(parcel.Unites.Count == 6) // Parmi toutes les parcelles on garde celle ayant 6 cases
                {
                    foreach(Case unit in parcel.Unites) // Puis on verifie laquelle contient la meilleure position selon Score et Priorité
                    {
                        int ParcellesAdjHere = ParcelAdj(0, unit);

                        if (ParcellesAdjHere > ParcellesAdjMax)
                        {
                            ParcellesAdjMax = ParcellesAdjHere;
                            Placement = unit;
                        }
                    }
                }
            }
            return Placement;
        }

        private void TestGraine(Case unit)
        {
            int last_index = Graines.Count - 1;
            PlacementGraine(unit, last_index);
            Graines.RemoveAt(last_index);
        }

        private void Reset(Case unit)
        {
            Placement.Remove(unit);
            int last_index = Graines.Count;
            unit.Graine = null;
            unit.Owner = null;
            unit.Parcel.EditOwnerParcelle();
            Graines.Add(new Graine(this));
        }

        public override void PlayGraine(Case emplacement)
        {
            // emplacement contient la case à jouer
            /* Map temporaire pour calcul des graines
             * 
             * Tactique de jeu
             * * Premiere case : Parcelle de 6 + case ayant le plus de parcelles adjacentes
             * * Idéees pour faire évoluer l'IA : 
                    Suite du jeu : 
             *            - Dim : Jouer parcelles impaires tant que nos parcelles paires ne sont pas contestées. Peut-être jouer de préférence
                                   dans les parcelles impaires, les cases qui ne soient pas alignées avec d'autres parcelles impaires (pour éviter d'ouvrir le jeu)

             *            - Alex : calcul de l'ensemble des cases jouables garder le score max + max de parcelles adjacentes
             * 
             TODO :
                - Stratégie simple basée sur :
                    - Premier placement sur la parcelle de plus grosse valeur (6) et sur la case avec le plus d'adjacences
                    - Dans la suite du jeu : jouer les parcelles impaires prioritairement, si impossible : jouer la paire qui a le plus de valeur tout en gardant
                    le plus d'adjacences
             */
            if(Serveur.Game.GameMode != ModeDeJeu.JoueurVsIAInt) // Jeu en ligne
            {
                unitPlay = emplacement;
                byte[] sentenceReceive = new byte[512];
                Serveur.Send(Encoding.ASCII.GetBytes($"A:{emplacement.Line}{emplacement.Column}"));
                Serveur.Receive(sentenceReceive);
                int last_index = Graines.Count - 1;
                if (Encoding.ASCII.GetString(sentenceReceive).StartsWith("VALI"))
                {
                    PlacementGraine(emplacement, last_index);
                }
                Graines.RemoveAt(last_index);
            }
            else // Jeu en interne
            {
                unitPlay = emplacement;
                int last_index = Graines.Count - 1;
                PlacementGraine(emplacement, last_index);
                Graines.RemoveAt(last_index);
            }
        }
    }
}
