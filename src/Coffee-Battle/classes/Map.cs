using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace CoffeeBattle
{
    public class Map
    {
        public const int MIN_RANGE = 0;
        public const int MAX_RANGE = 9;
        private string mapName;
        public string MapName
        {
            set
            {
                mapName = value;
            }
            get
            {
                return mapName;
            }
        }


        private List<Parcelle> parcelles;
        public List<Parcelle> Parcelles
        {
            get
            {
                return parcelles;
            }
            set
            {
                if (value != null)
                    parcelles = value;
            }
        }
        private byte[] bytes_map;
        public byte[] BytesMap
        {
            get
            {
                return bytes_map;
            }
            set
            {
                if (value.Length > 0 && value != null)
                    bytes_map = value;
            }
        }
        private List<List<Case>> map_grid;
        public List<List<Case>> MapGrid
        {
            get
            {
                return map_grid;
            }
            set
            {
                if (value != null)
                    map_grid = value;
            }
        }
        private Parcelle parcelToAdd;
        public Parcelle ParcelToAdd
        {
            get
            {
                return parcelToAdd;
            }
            set
            {
                parcelToAdd = value;
            }
        }
        private List<Case> caseClickable;
        public List<Case> CaseClickable
        {
            get
            {
                return caseClickable;
            }
            set
            {
                caseClickable = value;
            }

        }
        private Dictionary<int,int> nbParcel;
        public Dictionary<int,int> NbParcel
        {
            get
            {
                return nbParcel;
            }
            set
            {
                nbParcel = value;
            }
        }

        public Map()
        {
            InitNeutralGrid();
            Parcelles = new List<Parcelle>();
            CaseClickable = new List<Case>();
            NbParcel = new Dictionary<int, int>() { { 2, 4 }, { 3, 4 }, { 4, 5 }, { 6, 4 } };
        }

        public Map(ModeDeJeu mode, string carte)
        {
            /* Constructeur Map : constructeur permettant l'initialisation du plateau de jeu
             * IN : le mode de jeu choisi, ainsi que la carte de jeu
             * No OUT
             * Exemple : string Phatt = "67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|";
             *          Map(ModeDeJeu.JoueurVsJoueur, Phatt);
             */
            if (mode == ModeDeJeu.IAVsIA || mode == ModeDeJeu.JoueurVsIAExt)
            {
                bytes_map = new byte[512];
                try
                {
                    Serveur.Connect();
                    int sentence_length = Serveur.Receive(bytes_map);
                }
                catch (SocketException e) { throw e; }
                BytesMap = bytes_map;
            }
            else
            {
                BytesMap = Encoding.ASCII.GetBytes(carte);
            }
            InitMapGrid();
        }

        public Map(Map map)
        {
            CaseClickable = map.CaseClickable;
            Parcelles = map.Parcelles;
            MapGrid = map.MapGrid;
        }

        private void InitMapGrid()
        {
            /* Proc�dure initMapGrid : permet d'initialiser la grille des cases du jeu
             * No IN, No OUT
             * Appel�e dans le constructeur de Map
             */
            int id_line = 0;
            List<string> str_Line = new List<string>(Encoding.ASCII.GetString(bytes_map).Split('|'));
            MapGrid = new List<List<Case>>();
            str_Line.Remove(""); // Retrait du dernier item (cha�ne vide)
            foreach (string str in str_Line) // D�coupage en str puis en int
            {
                string[] unit = str.Split(':');
                List<Case> line_case = new List<Case>();
                int id_column = 0;
                foreach (string unite in unit)
                {
                    Case new_case;
                    if (Int32.TryParse(unite, out int number))
                    {
                        new_case = new Case(id_column, id_line, number);
                        line_case.Add(new_case);
                    }
                    id_column++;
                }
                MapGrid.Add(line_case);
                id_line++;
            }
            InitParcelle();
        }
        public void RemoveFromGrid(Case emplacement)
        {
            /* Proc�dure removeFromGrid : permet d'enlever une graine pos�e sur le plateau de jeu et de la r�cup�rer en main
             * IN : la case sur laquelle on r�cup�re la graine
             * No OUT
             * Exemple : removeFromGrid(new Case(3,4));
             */
            if (emplacement != null && emplacement.Graine != null)
            {
                emplacement.Owner.Graines.Add(emplacement.Graine);
                emplacement.Graine = null;
                emplacement.Graine.Location = null;
            }
        }

        private void InitNeutralGrid()
        {
            MapGrid = new List<List<Case>>();
            for (int line = 0; line < 10; line++)
            {
                List<Case> to_add = new List<Case>();
                for (int column = 0; column < 10; column++)
                {
                    Case added = new Case(column, line);
                    to_add.Add(added);
                }
                MapGrid.Add(to_add);
            }
        }
        public void EditCaseClickable() 
        {
            caseClickable = new List<Case>();
            foreach (Case unit in parcelToAdd.Unites)
                for (int to_column = 0; to_column < 2; to_column++)
                    for (int increment = 0; increment < 2; increment++)
                    {
                        int to_add = (increment == 0 ? 1 : -1);
                        try
                        {
                            Case unitBis = MapGrid[unit.Line + to_add * to_column][unit.Column + to_add * (to_column + 1) % 2];
                            if (!caseClickable.Contains(unitBis) && unitBis.Parcel == null)
                                caseClickable.Add(unitBis);
                        }
                        catch (Exception) { }
                    }
        }
        public void CalcCodedValue() // Assigne la valeur en bytes � chaque case de la parcelle
        {
            foreach (Case unit in parcelToAdd.Unites)
                for (int increment = 0; increment < 2; increment++)
                    for (int to_column = 0, to_add = (increment == 0 ? 1 : -1); to_column < 2; to_column++)
                    {
                        try
                        {
                            Case unitBis = MapGrid[unit.Line + to_add * to_column][unit.Column + to_add * (to_column + 1) % 2];
                            if (unitBis.Parcel != unit.Parcel)
                                unit.Value += (to_add * to_column == 1 ? 4 : (to_add * to_column == -1 ? 1 : (to_add * (to_column + 1) % 2 == 1 ? 8 : 2)));
                        }
                        catch (Exception)
                        {
                            unit.Value += (to_add * to_column == 1 ? 4 : (to_add * to_column == -1 ? 1 : (to_add * (to_column + 1) % 2 == 1 ? 8 : 2)));
                        }
                    }
        }
        public string Sentence()
        {
            string to_return = "";
            foreach (List<Case> line in MapGrid)
            {
                foreach (Case unit in line)
                {
                    if (unit.Column == 0)
                        to_return = String.Format("{0}{1}", to_return, unit.Value);
                    else
                        to_return = String.Format("{0}:{1}", to_return, unit.Value);
                }
                to_return = String.Format("{0}|", to_return);
            }
            return to_return;
        }
        public bool VerifParcelToAdd() // Verifie si les cases forment une parcelle unique
        {
            int i = 0;
            List<Case> to_check = new List<Case>() { ParcelToAdd.Unites[0] };
            while (i < to_check.Count)
            {
                Case unit = to_check[i];
                for (int increment = 0; increment < 2; increment++)
                    for (int to_column = 0, to_add = (increment == 0 ? 1 : -1); to_column < 2; to_column++)
                    {
                        try
                        {
                            Case unitBis = MapGrid[unit.Line + to_add * to_column][unit.Column + to_add * (to_column + 1) % 2];
                            if (!to_check.Contains(unitBis) && unitBis.Parcel == unit.Parcel)
                                to_check.Add(unitBis);
                        }
                        catch (Exception) { }
                    }
                i++;
            }
            return (to_check.Count == ParcelToAdd.Unites.Count);
        }

        private void InitParcelle()
        {
            /* Proc�dure initParcelle : permet l'initialisation des parcelles de jeu gr�ce � la grille de case
             * No IN, No OUT
             * Appel�e dans initMapGrid
             */
            Parcelles = new List<Parcelle>();
            foreach(List<Case> list_unite in MapGrid)
                foreach(Case unite in list_unite)
                    if (unite.Parcel == null)
                    {
                        Parcelle parcelle = new Parcelle();
                        parcelle.Unites.Add(unite);
                        unite.Parcel = parcelle;
                        ManageParcelleContain(ref parcelle);
                        Parcelles.Add(parcelle);
                    }
        }
        private void ManageParcelleContain(ref Parcelle parcelle)
        {
            /* Proc�dure manageParcelleContain : organise la construction de l'int�rieur d'une parcelle
             * IN par r�f�rence : la parcelle � organiser
             * No OUT
             * Appel�e dans initParcelle
             */
            int indice = 0;
            while (indice < parcelle.Unites.Count)
            {
                List<BorderCases> borderBrowse = parcelle.Unites[indice].InverseBorder();
                if (borderBrowse != null)
                    foreach (BorderCases border in borderBrowse)
                    {
                        Case caseTemp = parcelle.Unites[indice].SearchCase(border);
                        if (MapGrid[caseTemp.Line][caseTemp.Column].Parcel == null)
                        {
                            parcelle.Unites.Add(MapGrid[caseTemp.Line][caseTemp.Column]);
                            MapGrid[caseTemp.Line][caseTemp.Column].Parcel = parcelle;
                        }
                    }
                indice++;
            }
        }
    }
}
