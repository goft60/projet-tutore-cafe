﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeBattle
{
    public class Joueur
    {
        private string name;
        private int scoreParcelle, scoreBonus, scoreTotal;
        private List<Graine> graines;

        public List<Case> Placement { get; set;  }
        public List<Graine> Graines
        {
            set
            {
                if (value != null)
                    graines = value;
            }
            get
            {
                return graines;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != null && value.Length > 0)
                    name = value;
            }
        }
        public int ScoreParcelle
        {
            get
            {
                return scoreParcelle;
            }
            set
            {
                if ( value > -1 )
                    scoreParcelle = value;
            }

        }
        public int ScoreBonus
        {
            get
            {
                return scoreBonus;
            }
            set
            {
                if ( value > -1 )
                    scoreBonus = value;
            }
        }
        public int ScoreTotal
        {
            get
            {
                return scoreTotal;
            }
            set
            {
                if ( value > -1 )
                    scoreTotal = value;
            }
        }

        public Joueur(String name = null)
        {
            int nb_players = Serveur.Game.Players.Count;
            if (nb_players < 2)
            {
                Name = name ?? $"Joueur {nb_players}";
                ScoreParcelle = ScoreBonus = ScoreTotal = 0;
                Graines = new List<Graine>();
                Placement = new List<Case>();
                CreateGraines();
                Serveur.Game.Players.Add(this);
            }
        }
      
        protected void CreateGraines(int nb = Graine.NB_SEEDS)
        {
            if(nb > 0)
                for (int i = 0; i < nb; i++)
                {
                    Graine new_seed = new Graine(this);
                    Graines.Add(new_seed);
                }
        }
        public virtual void PlayGraine(Case emplacement)
        {
            if(Serveur.Game.GameMode == ModeDeJeu.JoueurVsJoueur)
            {
                if (emplacement.Owner == null)
                {
                    int last_index = Graines.Count - 1;
                    PlacementGraine(emplacement, last_index);
                    Graines.RemoveAt(last_index);
                }
            }
            else if (Serveur.Game.GameMode == ModeDeJeu.JoueurVsIAExt)
            {
                if (emplacement.Owner == null)
                {
                    byte[] sentenceReceive = new byte[512];
                    Serveur.Send(Encoding.ASCII.GetBytes($"A:{emplacement.Line}{emplacement.Column}"));
                    Serveur.Receive(sentenceReceive);
                    int last_index = Graines.Count - 1;
                    if (Encoding.ASCII.GetString(sentenceReceive).StartsWith("VALI"))
                    {
                        PlacementGraine(emplacement, last_index);
                    }
                    Graines.RemoveAt(last_index);
                }
            }
        }

        public string GetIaRemotePlay() 
        {
            byte[] sentenceReceive = new byte[512];
            Serveur.Receive(sentenceReceive);
            if (sentenceReceive[0] == 70)
                return null;
            string iaPlacement = Encoding.ASCII.GetString(sentenceReceive).Split(':')[1].Trim('\0');
            if(! ( iaPlacement.Contains("ENCO") || iaPlacement.Contains("FINI")))
            {
                Serveur.Receive(sentenceReceive);
                iaPlacement += Encoding.ASCII.GetString(sentenceReceive).Trim('\0');
            }

            Case unit = Serveur.Game.Map.MapGrid[Int16.Parse(iaPlacement[0].ToString())][Int16.Parse(iaPlacement[1].ToString())];
            int last_index = Graines.Count - 1;
            PlacementGraine(unit, last_index);
            Graines.RemoveAt(last_index);
            return $"{iaPlacement[2]}:{iaPlacement[0]}{iaPlacement[1]}";
        }
        protected void PlacementGraine(Case emplacement, int last_index)
        {
            Placement.Add(emplacement);
            Graines[last_index].Location = emplacement;
            emplacement.Graine = Graines[last_index];
            emplacement.Owner = this;
            emplacement.Parcel.EditOwnerParcelle();
        }
        public void CountPoints()
        {
            // Méthode comptant le nombre de points gagné pendant le tour
            ScoreParcelle = Serveur.Game.CountOwnerParcelles(this);
            ScoreBonus = Serveur.Game.CountOwnerCasesAdj(this);
            ScoreTotal = ScoreParcelle + ScoreBonus;
        }

    }
}
