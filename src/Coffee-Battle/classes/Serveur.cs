using System.Net;
using System.Net.Sockets;
using System;


namespace CoffeeBattle
{
    public static class Serveur
    {
        private static Socket sockets_server;
        public static Socket Sockets
        {
            get
            {
                return sockets_server;
            }
            set
            {
                if (value != null)
                    sockets_server = value;
            }
        }
        private static Partie game;
        public static Partie Game
        {
            get
            {
                return game;
            }
            set
            {
                if (value != null)
                    game = value;
            }
        }
        private const AddressFamily IP_TYPE = AddressFamily.InterNetwork;
        private const SocketType SOCKET_TYPE = SocketType.Stream;
        private const ProtocolType PROTOCOL = ProtocolType.Tcp;
        private const string HOST_IP = "127.0.0.1";
        private const int PORT = 1213;
        public static string GetHostIp()
        {
            return HOST_IP;
        }
        public static int GetPort()
        {
            return PORT;
        }

        public static void Connect()
        {
            /* Proc�dure statique Connect : permet la connexion au serveur via son IP et son port
             * No IN, No OUT
             * Exceptions : ArgumentNullException, ArgumentOutOfRangeException, SocketException, ObjectDisposedException, NotSupportedException, InvalidOperationException
             * Exemple : Serveur.Connect();
             */
            sockets_server = new Socket(IP_TYPE, SOCKET_TYPE, PROTOCOL);
            sockets_server.Connect(HOST_IP, PORT);
        }
        public static int Send(byte[] buffer)
        {
            /* Fonction statique Send : permet d'envoyer une trame d'octets au serveur
             * IN : Tableau d'octets contenant la trame � envoyer
             * OUT : Nombre entier d'octets envoy� par la socket
             * Exceptions : ArgumentNullException, SocketException, ObjectDisposedException
             * Exemple : int length = Serveur.Send(Encoding.ASCII.GetBytes("Hello World !"));
             */
            if (buffer != null && sockets_server != null)
                sockets_server.Send(buffer);
            return buffer.Length;
        }
        public static int Receive(byte[] buffer)
        {
            /* Fonction statique Receive : permet de recevoir une trame d'octets du serveur
             * IN : Tableau d'octets qui sera rempli par le message du serveur
             * OUT : Nombre entier d'octets re�u par la socket
             * Exceptions : ArgumentNullException, SocketException, ObjectDisposedException, SecurityException
             * Exemple : bytes[] sentenceReceive = new Bytes[512];
             *           int length = Serveur.Receive(sentenceReceive);
             */
            if (buffer != null && sockets_server != null)
                sockets_server.Receive(buffer);
            return buffer.Length;
        }
        public static void Shutdown(SocketShutdown method)
        {
            /* Proc�dure statique Shutdown : permet d'interrompre les flux de donn�es
             * IN : 
             * No OUT
             * Exceptions : SocketException, ObjectDisposedException
             * Exemple : try { Serveur.Shutdown(SocketShutdown.Both); }
             *           finally { Serveur.Close(); }
             */
            if (sockets_server != null)
                sockets_server.Shutdown(method);
        }
        public static void Close()
        {
            /* Proc�dure statique Close : permet de fermer la connexion au serveur
             * No IN, No OUT
             * Exemple : Serveur.Close();
             */
            if (sockets_server != null)
                sockets_server.Close();
        }

        public static void BeginGame(ModeDeJeu mode, string map)
        {
            /* Proc�dure statique BeginGame : permet de d�marrer une partie de jeu
             * IN : le mode de jeu choisi, ainsi que la carte de jeu
             * No OUT
             * Exemple : string Phatt = "67:69:69:69:69:69:69:69:69:73|74:3:9:7:5:13:3:1:9:74|74:2:8:7:5:13:6:4:12:74|74:6:12:7:9:7:13:3:9:74|74:3:9:11:6:13:7:4:8:74|74:6:12:6:13:11:3:13:14:74|74:7:13:7:13:10:10:3:9:74|74:3:9:11:7:12:14:2:8:74|74:6:12:6:13:7:13:6:12:74|70:69:69:69:69:69:69:69:69:76|";
             *          Serveur.BeginGame(ModeDeJeu.JoueurVsJoueur, Phatt);
             */
            Game = new Partie(mode, map);
        }
    }
}
