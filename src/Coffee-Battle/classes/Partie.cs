﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeBattle
{
    public class Partie
    {
        private Map map;
        private readonly int NB_PLAYER = 2;
        private List<Joueur> players;
        private Joueur current_player;
        private int tour;
        private ModeDeJeu gamemode;

        public Joueur CurrentPlayer
        {
            get
            {
                return current_player;
            }
            set
            {
                if (value != null)
                    current_player = value;
            }
        }
        public int Tour
        {
            get
            {
                return tour;
            }
            set
            {
                if (value >= 0)
                    tour = value;
            }
        }
        public ModeDeJeu GameMode
        {
            get
            {
                return gamemode;
            }
            set
            {
                if (value != ModeDeJeu.Unknown)
                    gamemode = value;
            }
        }

        public Map Map
        {
            get
            {
                return map;
            }
            set
            {
                if (value != null)
                    map = value;
            }
        }
        public List<Joueur> Players
        {
            set
            {
                if (value != null && Players.Count < NB_PLAYER)
                    players = value;
            }
            get
            {
                return players;
            }
        }

        public Partie(ModeDeJeu gamemode, string map)
        {
            Map = new Map(gamemode, map);
            GameMode = gamemode;
            players = new List<Joueur>();
            Tour = 1;
        }
        public void NextRound()
        {
            if (CurrentPlayer == Players[0])
                CurrentPlayer = Players[1];
            else
            {
                CurrentPlayer = Players[0];
                tour++;
            }
                
        }
        public int CountOwnerParcelles(Joueur joueur)
        {
            int nb = 0;
            foreach (Parcelle parcel in Map.Parcelles)
                if (parcel.Proprio == joueur)
                    nb+= parcel.Unites.Count;
            return nb;
        }
        public int CountOwnerCasesAdj(Joueur joueur)
        {
            int nb = 0, max = 0;
            List<Case> caseVerifie = new List<Case>();
            foreach (Case unit in joueur.Placement)
            {
                nb = ParcoursProfondeur(unit, ref caseVerifie);
                if (max < nb)
                    max = nb;
            }
            return max;
        }
        private int ParcoursProfondeur(Case unit, ref List<Case> caseVerifie)
        {
            int nb = 1;
            caseVerifie.Add(unit);
            Case unitBis;
            if (unit.Line <9 && (unitBis = Serveur.Game.Map.MapGrid[unit.Line + 1][unit.Column]).Owner == unit.Owner && !caseVerifie.Contains(unitBis))
                nb += ParcoursProfondeur(unitBis, ref caseVerifie);
            if (unit.Column <9 && (unitBis = Serveur.Game.Map.MapGrid[unit.Line][unit.Column + 1]).Owner == unit.Owner && !caseVerifie.Contains(unitBis))
                nb += ParcoursProfondeur(unitBis, ref caseVerifie);
            if (unit.Line>0 && (unitBis = Serveur.Game.Map.MapGrid[unit.Line - 1][unit.Column]).Owner == unit.Owner && !caseVerifie.Contains(unitBis))
                nb += ParcoursProfondeur(unitBis, ref caseVerifie);
            if (unit.Column > 0 && (unitBis = Serveur.Game.Map.MapGrid[unit.Line][unit.Column - 1]).Owner == unit.Owner && !caseVerifie.Contains(unitBis))
                nb += ParcoursProfondeur(unitBis, ref caseVerifie);
            return nb;
        }
        public void InitPlayers(string name1, string name2)
        {
            new Joueur(name1);
            new Joueur(name2);
            CurrentPlayer = Players[0];
        }

        public void InitIa(string name1, string name2)
        {
            new IA(name1);
            new IA(name2);
            CurrentPlayer = Players[0];
        }

        public List<Case> CaseJouable(Case unit)
        {
            List<Case> jouable = new List<Case>();
            int mem = 0;
            for(int LineOrColumn = 0; LineOrColumn <2; LineOrColumn ++)
                for (int indice = 0; indice < 10; indice++)
                {
                    Case unitBis = (LineOrColumn==0 ? Serveur.Game.Map.MapGrid[indice][unit.Column] : Serveur.Game.Map.MapGrid[unit.Line][indice]);
                    if (unitBis.Owner == null && unitBis.Type == TypeCases.Available && unitBis.Parcel != unit.Parcel)
                    {
                        jouable.Add(unitBis);
                        mem++;
                    }
                }
            return (mem == 0 ? null : jouable);
        }
        public void SortScorePlayers()
        {
            if (Players[0].ScoreTotal < Players[1].ScoreTotal)
            {
                Players.Sort();
            }
        }
    }
}
