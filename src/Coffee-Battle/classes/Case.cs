﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeBattle
{
    public class Case
    {
        private int column, line, codedValue;
        private Graine graine;
        public Graine Graine
        {
            set
            {
                graine = value;
            }
            get
            {
                return graine;
            }
        }
        public int Value
        {
            get
            {
                return codedValue;
            }
            set
            {
                if (value >= 0)
                {
                    codedValue = value;
                    InitType();
                }
            }
        }
        public int Column
        {
            get
            {
                return column;
            }
            set
            {
                if (value >= Map.MIN_RANGE && value <= Map.MAX_RANGE)
                    column = value;
            }
        }
        public int Line
        {
            get
            {
                return line;
            }
            set
            {
                if (value >= Map.MIN_RANGE && value <= Map.MAX_RANGE)
                    line = value;
            }
        }
        public Joueur Owner
        {
            get
            {
                return (Graine?.Owner);
            }
            set
            {
                if(Graine != null)
                    Graine.Owner = value;
            }
        }
        private TypeCases type;
        public TypeCases Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
        private List<BorderCases> borders;
        public List<BorderCases> Borders
        {
            get
            {
                return borders;
            }
            set
            {
                if (value != null)
                    borders = value;
            }
        }
        private Parcelle parcel;
        public Parcelle Parcel
        {
            get
            {
                return parcel;
            }
            set
            {
                parcel = value;
            }
        }
     


        public Case(int x, int y, int codedValue, TypeCases type = TypeCases.Unknown, List<BorderCases> borders = null, Parcelle parcelle = null)
        {   
            /* Constructeur Case : initialise tous les attributs de la case
             * IN : l’ensemble des paramètres de la case dont trois sont obligatoire sa colonne, sa ligne ainsi que sa valeur
             * No OUT
             * Exemple : Case(0,0,67) // Crée une case en coordonnées 0;0 avec une valeur de 67 
             */
            Column = x;
            Line = y;
            Type = type;
            Borders = borders;
            Value = codedValue;
            Parcel = parcelle;
        }
        public Case(int x, int y)
        {
            Column = x;
            Line = y;
            Value = 0;
        }
  
        public Case()
        {
            Column = 0;
            Line = 0;
            Value = 0;
        }
        private void InitType()
        {
            /* Procédure InitType : initialise le type de la case
             * No IN, No OUT
             * Appelé quand une valeur est affecté à l’attribut CodedValue 
             */
            Array enums = Enum.GetValues(typeof(TypeCases));
            int id_enum = 2;
            while(Type == TypeCases.Unknown && id_enum >= 0)
            {
                TypeCases typetest = (TypeCases) enums.GetValue(id_enum);
                if (Value >= (int) typetest)
                    Type = typetest;
                id_enum--;
            }
            InitBorders();
        }
        private void InitBorders()
        {
            /* Procédure InitBorders : initialise les bordures de la case
             * No IN, No OUT
             * Appelé dans InitType
             */
            int val_border = Value - (int)Type;
            Array enums = Enum.GetValues(typeof(BorderCases));
            Borders = new List<BorderCases>();
            int id_enum = 3;
            while(val_border > 0 && id_enum >= 0)
            {
                BorderCases border = (BorderCases)enums.GetValue(id_enum);
                int border_int = (int)border;
                if (val_border >= border_int)
                {
                    borders.Add(border);
                    val_border -= border_int;
                }
                id_enum--;
            }
        }

        public List<BorderCases> InverseBorder()
        {
            /* Fonction InverseBorder : retourne les bordures non présentes sur la case
             * No IN, OUT : une liste contenant les bordures
             * Exemple : List<BorderCases> inverse = InverseBorder(); // si la case à une bordure au nord et à l’est alors cela retournera ouest et sud 
             */
            List<BorderCases> borderInverse = new List<BorderCases>();
            foreach (BorderCases border in Enum.GetValues(typeof(BorderCases)))
                if (border != BorderCases.Unknown)
                {
                    bool flag = true;
                    foreach (BorderCases borderHere in Borders)
                        if (borderHere == border)
                            flag = false;
                    if (flag)
                        borderInverse.Add(border);
                }
            return borderInverse;
        }

        public Case SearchCase ( BorderCases border )
        {
            /* Fonction SearchCase : Fonction retournant la case adjacente en fonction de la bordure 
             * IN : BorderCases : type de la bordure (bordure nord, sud, ouest, est)
             * OUT : Case adjacente trouvée selon la bordure entrée
             * Utile pour déterminer les parcelles 
             */
            Case unite;
            int val_border = (int)border;
            if (val_border == 1) // North une case vers le haut
                unite = new Case(Column, Line-1);
            else if (val_border == 2) // West une case à gauche
                unite = new Case(Column-1, Line);
            else if (val_border == 4) // South une case vers le bas
                unite = new Case(Column, Line+1);
            else // East une case à droite
                unite = new Case(Column+1, Line);
            return unite;
        }
    }
}
