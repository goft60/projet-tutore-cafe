﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeBattle
{
    public enum BorderCases
    {
        Unknown = -1,
        North = 1,
        West = 2,
        South = 4,
        East = 8
    }
}
