﻿    using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeBattle
{
    public enum ModeDeJeu
    {
        Unknown = 0,
        JoueurVsJoueur = 1,
        JoueurVsIAExt = 2,
        JoueurVsIAInt =3,
        IAVsIA = 4
    }
}
