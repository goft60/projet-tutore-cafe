﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoffeeBattle
{
    //
    // Constantes définissant le type des unités
    //  Available : Une graîne de café peut être posée
    //  Forest : L'unité est une forêt
    //  Water : L'unité est une mer
    public enum TypeCases
    {
        Unknown = -1,
        Available = 0,
        Forest = 32,
        Water = 64
    }
}
